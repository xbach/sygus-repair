package extsolvers.metasearch.selection

import engines.coreast.CAST

import scala.util.Random

/**
  * Created by dxble on 7/15/16.
  */
case class TournamentSelection(population: List[CAST], rand: Random) extends SelectionScheme[CAST] {
  //Prefer smaller size
  private def compareSize(e1: CAST, e2: CAST): Int ={
    if(e1.size() > e2.size())
      return -1
    else return 1
  }

  //Prefer bigger size
  private def compareSize2(e1: CAST, e2: CAST): Int ={
    if(e1.size() > e2.size())
      return 1
    else return -1
  }

  override def select(): CAST = {
    var selectionSize = 2
    if(population.size < 2){
      selectionSize = 1
    }
    val compare: (CAST, CAST) => Int = if(population.size > 3) compareSize else compareSize2
    SelectionScheme.tournamentSelection[CAST](population, selectionSize, rand, true, compare)
  }
}
case class StochasticUniversalSampling(population: List[CAST], rand: Random) extends SelectionScheme[CAST] {
  private def getFitness(e: CAST): Double ={
    e.size().toDouble/50
  }

  private def compilable(e: CAST): Boolean={
    return true
  }

  override def select(): CAST = {
    val selectionSize = 1
    /*if(population.size < 2){
      selectionSize = 1
    }*/
    SelectionScheme.stochasticUniversalSampling(population, selectionSize, rand, getFitness, compilable)(0)
  }
}