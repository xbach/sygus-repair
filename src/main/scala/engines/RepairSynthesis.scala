package engines

import smt.SMTParser
import common.Utils
import AngelicFix._
import java.io.File

import config.ConfigOptions

import scala.collection.JavaConverters._
import smt.SMTUtils
import engines.features._
import engines.utils.CASTUtils
import engines.coreast._
import org.smtlib.IExpr
import org.smtlib.command.C_assert

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
/**
  * Created by dxble on 6/12/16.
  */
object RepairSynthesis {
  var synthesisSuccess = false

  def generatePatch(extractedDir: String,
                    angelicForestRaw: AngelicForest, solver: Synthesizers, outputFile: String, config: SynthesisConfig): Unit = {
    //TODO I need to be more clear from where I take ids: files or environment variable
    val suspiciousIds = (new File(extractedDir)).listFiles.filter(f => """.*\.smt2$""".r.findFirstIn(f.getName).isDefined).map(_.getName.dropRight(".smt2".length)).toList
    val stmtIDIntConst = new scala.collection.mutable.HashMap[Int, scala.collection.mutable.HashSet[String]]
    val stmtIDBoolConst = new scala.collection.mutable.HashMap[Int, scala.collection.mutable.HashSet[String]]
    val idASTMap = ListBuffer[(Int, CAST)]()
    val suspicious = suspiciousIds.zipWithIndex.map({
      case (id, index) =>
        val script = SMTParser.parseFile(new File(extractedDir + "/" + id + ".smt2"))
        val expr = script.commands().asScala.toList.map({
          case asrt: C_assert => {
            val incrIndex = index + 1
            val constsVisitor = new ConstsCollector
            asrt.expr().accept(constsVisitor)
            import scala.collection.JavaConversions._
            val intConsts = stmtIDIntConst.get(incrIndex).getOrElse(new scala.collection.mutable.HashSet[String]())
            for (i: String <- constsVisitor.intConsts){
              intConsts.add(i)
            }
            stmtIDIntConst += (index + 1 -> intConsts)
            val boolConsts = stmtIDBoolConst.get(incrIndex).getOrElse(new scala.collection.mutable.HashSet[String]())
            for (i: String <- constsVisitor.boolConsts){
              boolConsts.add(i)
            }
            stmtIDBoolConst += (index + 1 -> boolConsts)
            val smt = new SMTExprVisitor
            asrt.expr().accept(smt)
            idASTMap += ((index + 1, smt.ast))
            asrt.expr()
          }
        }).apply(0)

        (index + 1, expr)
    })

    val realSuspicousIds = "" :: suspiciousIds
    //FIXME: (index + 1) is magic, without which it does not work (probably, I used id=0 for something else)
    //val (angelicForest, repairableBindings) =  edu.nus.mrepair.klee.RCGenerator.correctTypes(angelicForestRaw, suspicious,realSuspicousIds)

    val angelicForest = angelicForestRaw
    val oldExprs = idASTMap.map{
      case (index, cast) => {
        (realSuspicousIds(index), cast)
      }
    }
    //FIXME: when I support instances, it should be done using repairable objects
    //val oldExprs = repairableBindings.map({
    //  case (ProgramVariable(name, _), pfe, _, _, _) => (realSuspicousIds((name.split("#")(1)).toInt), pfe)
    //})


    val stmtLineIntConsts = stmtIDIntConst.foldLeft(new scala.collection.immutable.HashMap[String, scala.collection.mutable.HashSet[String]]){
      (res, i) =>{
        val stmtLine = realSuspicousIds(i._1)
        res + (stmtLine -> i._2)
      }
    }

    val stmtLineBoolConsts = stmtIDBoolConst.foldLeft(new scala.collection.immutable.HashMap[String, scala.collection.mutable.HashSet[String]]){
      (res, i) =>{
        val stmtLine = realSuspicousIds(i._1)
        res + (stmtLine -> i._2)
      }
    }

    //additionalVars is map: stmtid -> (varsNotinOldExpr, varsValueUnchanged)
    val additionalVars  = collectAdditionVars2(angelicForest, suspicious, realSuspicousIds)
    //asts is map: stmtid -> (core ast, collection of vars that have unchanged value, collection of vars having changed Value)
    val asts = CASTUtils.transformASTs(idASTMap, realSuspicousIds, additionalVars)
    val enoughVarsFeature = new EnoughVarsFeature(asts)
    // Adding feature that can be used for each solver
    if(ConfigOptions.syntaxFeature || ConfigOptions.onlyFeature == 4)
      solver.addFeature(new MajorVarNotChange)
    if(ConfigOptions.semanticFeature || ConfigOptions.onlyFeature == 3)
      solver.addFeature(new NoDupOnSameOperator)
    if(ConfigOptions.syntaxFeature || ConfigOptions.onlyFeature > 0) {
      solver.addFeature(enoughVarsFeature)
      if(ConfigOptions.onlyFeature == 2)
        solver.addFeature(new CosineSimilarityFeature(0.69)) //0.69 //0.7

      if(ConfigOptions.onlyFeature == 7)
        solver.addFeature(new VarLocationChange(0.5)) //0.5

      if(ConfigOptions.onlyFeature == 5)
        solver.addFeature(new GumtreeDiffFeature(4.0)) //4.0 //3.0
    }
    //solver.addFeature(new ModelCounterCloserFeature("/home/dxble/Downloads/latte-integrale-1.7.3/latte-int-1.7.3/code/latte/count", -100, 100))

    def doExcludeVariables(config: SynthesisConfig): mutable.HashMap[String, List[String]] ={
      if(config.spaceReduction) {
        //if(solver.isInstanceOf[MetaSearchSynth]){
        //  return excludeOnlyVarsNotInOldExpr(config,additionalVars, asts)
        //}else {
          return excludeTheseVars(config, additionalVars, asts)
        //}
      }
      else
        return new mutable.HashMap[String, List[String]]()
    }

    val excludeVars = doExcludeVariables(config)
    val sygusConverter = new AF2SyGus(angelicForest, config, stmtLineIntConsts, stmtLineBoolConsts, solver.isInstanceOf[CVC4Synth], excludeVars, asts)
    val sygusInput = sygusConverter.convertAF2SyGus()
    var refinedInput = sygusInput
    //var notWantedSol = ""
    var execCount = 0 // number of execution times of: solver.solve()
    var foundOKSolution = false
    var timeout = false
    //var failFirstTry = false
    if(solver.isInstanceOf[MetaSearchSynth]){
      var allOrigExprs = ""
      //val old = oldExprs.toMap
      sygusConverter.funSignatures.foreach(line2FunSig => {
        var originalExpr = "(original-expr "
        val (stmtLine, funSig) = line2FunSig
        originalExpr += funSig + " "
        //val (ast, _) = asts.get(stmtLine).getOrElse(null)
        //val changedPart = CASTUtils.getChangedPart(ast)

        val changedPart = sygusConverter.funChangedParts.getOrElse(stmtLine, null)
        originalExpr += changedPart.smtString()
        originalExpr += ")\n"
        allOrigExprs += originalExpr

      })
      refinedInput = refinedInput.replace("(check-synth)", allOrigExprs)
      refinedInput += "\n" + "(check-synth)"
    }

    val sygusFile = outputFile + ".sygus"
    solver.setSyGusFile(sygusFile)
    assert(solver.getSyGusFile() != "")
    Utils.writeToFile(sygusFile, refinedInput)

    while (execCount < config.solverBound && !foundOKSolution && !timeout) {

      val patchResults = solver.solve()

      patchResults match {
        case (_, true, _) => {
          println("TIMEOUT")
          timeout = true
        }

        case (solution, false, success) =>
          if (success == false) {
            //failFirstTry = true
            if(execCount == config.solverBound - 1)
              println("FAIL")
            else {
              if(config.simplification) {
                refinedInput = Strategy.randomlyDropConstraints(refinedInput, config, solver.getRandom())
                Utils.writeToFile(sygusFile, refinedInput)
              }
            }
          }
          else {
            solution match {
              case OKSolution(d) => {
                val old = oldExprs.toMap
                val synthesizedFuncs = d.split("\n")
                val changes = synthesizedFuncs.foldLeft("")((res1, func) => {
                  val sp = func.split(":")
                  val stmtId = sp(0).split("_")(1) //sp(0) = f_stmtid (line number)
                  val oldExpr = old(stmtId)
                  val fixExpr = sp(1)
                  val oldAST = asts.get(stmtId) match {
                    case None => throw new RuntimeException("Why?")
                    case Some(pair) => {
                      val (ast, _,_) = pair
                      ast
                    }
                  }
                  var transformedFixedExp = ""
                  if (decideStructureReuse(config, oldAST) && config.spaceReduction) {
                      transformedFixedExp = CASTUtils.transformFixExprWrapper(oldAST, fixExpr)
                  } else {
                    transformedFixedExp = fixExpr
                  }
                  //res1 + stmtId + "\n" + RCGenerator.beautifyResult(oldExpr) + "\n" + transformedFixedExp + "\n"
                  res1 + stmtId + "\n" + oldExpr + "\n" + transformedFixedExp + "\n"
                })
                println("SUCCESS")
                synthesisSuccess = true
                foundOKSolution = true
                Utils.writeToFile(outputFile, changes)
              }

              case TrivialSolution(d) =>{
                val synthesizedFuncs = d.split("\n")
                val notWantedSol = synthesizedFuncs.foldLeft("")((res, func) => {
                  if(func.contains("Trivial")){
                    res+"\n"+func.split(":")(1)+";NOT_WANTED"+"\n" //constraint of not wanted solution
                  }else{
                    res
                  }
                })
                refinedInput = addNotWantedSolution(refinedInput, notWantedSol)
                Utils.writeToFile(sygusFile, refinedInput)
              }
              case NotFoundSolution() => {
                if(execCount == config.solverBound - 1)
                  println("FAIL")
                else{
                  // Otherwise, do nothing and let the loop runs. Note that we expect the random to make something special/different
                  //solver.getRandom().setSeed(System.currentTimeMillis())
                }
              }
            }

          }
      }
      execCount += 1
    }//end while loop
  }

  def addNotWantedSolution(sygusInput: String, notWantedSol: String): String ={
    if(notWantedSol != "")
      return sygusInput.replace("(check-synth)", notWantedSol).concat("\n(check-synth)")
    else
      return sygusInput
  }

  def decideStructureReuse(config: SynthesisConfig, oldAST: CAST): Boolean ={
    def containComplicatedStructure(ast: CAST): Boolean = {
      ast match {
        case e: BoolExpr2Change => containComplicatedStructure(e.op)
        case e: IntExpr2Change => containComplicatedStructure(e.op)
        case e: BoolExprNoChange => containComplicatedStructure(e.op)
        case e: IntExprNoChange => containComplicatedStructure(e.op)
        case e: Not => containComplicatedStructure(e.opr)
        case Equal(IntExpr2Change(IntConst(_)), IntExpr2Change(IntConst(_))) => false
        case And(_,_)|Or(_,_)|Not(_)|GTE(_,_)|LTE(_,_)|Greater(_,_)|LessThan(_,_)|Equal(_,_)|Plus(_,_)|Minus(_,_)|Div(_,_)|Mult(_,_) => true
        case _ => false
      }
    }
    return config.reuseStructure && containComplicatedStructure(oldAST)
  }

  def collectAdditionVars(angelicForest: AngelicForest,
                          suspicious: List[(Int, IExpr)],
                          suspiciousIds: List[String]) ={
    val usedVariables = suspicious.map({
      case (stmtId, expr) =>
        (stmtId, SMTUtils.collectVarNames(expr))
    }).toMap

    val additionalVariables = angelicForest.map({
      case (_, aps) =>
        aps.map({
          case ap =>
            val x = ap.map({
              case AngelicValue(ctx, _, stmtId, _) =>
                ctx.map({ case varval => (stmtId, AngelicFix.getName(varval)) }) //suspiciousIds.indexOf(stmtid)
            })
            x.flatten
        }).flatten
    }).flatten.groupBy(_._1).map({
      case (stmtId, vars) =>
        (stmtId, vars.toList.map(_._2).filter({ case n => !usedVariables(suspiciousIds.indexOf(stmtId)).contains(n)}).distinct)
    })
    additionalVariables
  }

  def collectAdditionVars2(angelicForest: AngelicForest,
                          suspicious: List[(Int, IExpr)],
                          suspiciousIds: List[String]): Map[String, (List[String], List[String])] ={
    val usedVariables = suspicious.map({
      case (stmtId, expr) =>
        (stmtId, SMTUtils.collectVarNames(expr))
    }).toMap

    val flattenedTuples = angelicForest.map({
      case (_, aps) =>
        val x2 = aps.map({
          case ap =>
            val x = ap.map({
              case AngelicValue(ctx, _, stmtId, _) =>
                ctx.map({ case varval => (stmtId, AngelicFix.getName(varval), getValue(varval)) }) //suspiciousIds.indexOf(stmtid)
            })
            x.flatten
        })
        x2.flatten
    }).flatten
    val additionalVariables = flattenedTuples.groupBy(_._1).map({// group by stmtId
      case (stmtId, vars) =>
        val listVars = vars.toList
        val varsNotInOldExprs = listVars.map(_._2).filter({ case n => !usedVariables(suspiciousIds.indexOf(stmtId)).contains(n)}).distinct
        // Bachle: new updates
        if(/*angelicForest.size == 1*/ false){
          (stmtId, (varsNotInOldExprs, List[String]()))
        } else {
          val varsUnchangedValues = listVars.groupBy(_._2).foldLeft(List[String]())({
            (res, eachPair) => {
              val (varName, groupByVarname) = eachPair
              val allValuesOfVarName = groupByVarname.toSet
              if (allValuesOfVarName.size == 1 && groupByVarname.size > 1) {
                //List of varName which has only one value in EACH statement to be synthesized
                res :+ varName
              } else
                res
            }
          })
          if(usedVariables(suspiciousIds.indexOf(stmtId)).toSet.diff(varsUnchangedValues.toSet).size == 0)
            (stmtId, (varsNotInOldExprs, List[String]()))
          else
            (stmtId, (varsNotInOldExprs, varsUnchangedValues))
        }
    })
    additionalVariables
  }

  def getValue(v: VariableValue): String = {
    v match {
      case IntVal(n, v)  => v.toString
      case BoolVal(n, v) => v.toString
      case CharVal(n, v) => v.toString
    }
  }

  /**
    * TODO: currently will exclude whatever we are given. We will need to choose what to exclude later
    *
    * @param additionalVars
    * @return
    */
  def excludeTheseVars(config: SynthesisConfig, additionalVars: Map[String, (List[String], List[String])], asts: mutable.HashMap[String, (CAST, mutable.HashSet[String], mutable.HashSet[String])]):  mutable.HashMap[String, List[String]] ={
    additionalVars.foldLeft(new mutable.HashMap[String, List[String]]()){
      (res, eachPair) => {
        val (stmtID, vars2Exclude) = eachPair
        val (varsNotInOldExprs, varsUnchangedValues) = vars2Exclude
        val existingVars = res.get(stmtID).getOrElse(List[String]())
        val (ast,varsNoChangeValue,_) = asts.get(stmtID) match {
          case None => throw new RuntimeException("Unexpected! Return none ast for stmt line: "+stmtID)
          case Some(pair) => pair
        }

        //val newVars = existingVars ++ varsNotInOldExprs ++ varsUnchangedValues
        if(decideStructureReuse(config, ast)) {
          // Bachle: new updates: keep all vars in old exp if the whole exp needs be changed. This makes sure the grammar well-defined
          val excludeByReuseStructure1 = existingVars ++ varsNotInOldExprs
          val excludeByReuseStructure2 = /*if(ast.isInstanceOf[Exp2Change]) List[String]() else*/ varsNoChangeValue.toList
          val excludeByReuseStructure = excludeByReuseStructure1 ++ excludeByReuseStructure2
          val unique = excludeByReuseStructure.toSet.toList
          res.update(stmtID, unique)
        }else{
          //If we dont reuse structure, we do not exclude vars that are not in old Expressions
          //This allows flexibility
          val excludeByNotReuseStructure = existingVars ++ varsNoChangeValue.toList
          val unique = excludeByNotReuseStructure.toSet.toList
          res.update(stmtID, unique)
        }
        res
      }
    }
  }

  def excludeOnlyVarsNotInOldExpr(config: SynthesisConfig, additionalVars: Map[String, (List[String], List[String])], asts: mutable.HashMap[String, (CAST, mutable.HashSet[String])]) :  mutable.HashMap[String, List[String]] ={
    additionalVars.foldLeft(new mutable.HashMap[String, List[String]]()) {
      (res, eachPair) => {
        val (stmtID, vars2Exclude) = eachPair
        val (varsNotInOldExprs, varsUnchangedValues) = vars2Exclude
        val (ast,varsNoChangeValue) = asts.get(stmtID) match {
          case None => throw new RuntimeException("Unexpected! Return none ast for stmt line: "+stmtID)
          case Some(tuple) => (tuple._1,tuple._2)
        }
        if(decideStructureReuse(config, ast)) {
          res.get(stmtID) match {
            case None => res.update(stmtID, varsNotInOldExprs)
            case Some(e) => res.update(stmtID, e ::: varsNotInOldExprs)
          }
        }else{
          //res
        }
        res
      }
    }
  }
  /**
    * Exclude those variables that have unchanged values across angelix forest since these vars
    * may not be what we are interested in.
    *
    * @param additionalVars
    * @return
    */
  /*def excludeVarsUnchangedValues(angelicForest: AngelicForest): Map[String, List[String]] ={
    //map of: stmtid -> inner map: varName -> set: value
    angelicForest.foldLeft(new mutable.HashMap[String, mutable.HashMap[String, mutable.HashSet[String]]]()){
      (res, testIDAglPaths) => {
        val (testID, aps) = testIDAglPaths
        aps.foldLeft()
      }
    }
  }*/


}
