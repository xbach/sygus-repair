package extsolvers.metasearch.searcher

import extsolvers.common.UnrolledGrammar
import engines.coreast._
import extsolvers.utils.ExtUtils
import synthlib2parser._

import scala.collection.{mutable, _}
import scala.util.parsing.input.Position
/**
  * Created by dxble on 7/17/16.
  */
/**
  * This should be called on each synth-fun cmd
  *
  * @param sort
  * @param unrolledGrammar
  */
class ExpEnumerator(unrolledGrammar: UnrolledGrammar, var limitSize: Int, var keepPushingScope: Boolean, pos: Position) extends ASTVisitorBase("ExpressionEnumerator"){
  var sort: SortExp = null
  var expSize: Int = 1
  var currentMaxSize = limitSize
  var currentScope: String = null
  //ScopeName -> [size -> exps of this size]
  val expStack = new mutable.HashMap[String, mutable.HashMap[Int, mutable.HashSet[CAST]]]()
  val scopeStack = new mutable.Stack[NTDef]()
  val scopeMaxsize = new mutable.HashMap[String, Int]()
  var endNTDefs = false
  var scopeStackChanging = true

  private def isScopeSymbol(symbol: String): Boolean ={
    unrolledGrammar.scope2Type.get(symbol) match {
      case None => false
      case _ => true
    }
  }

  override def visitSynthFunCmd(synthFunCmd: SynthFunCmd) = {
    synthFunCmd.grammarRules.foreach(rule => {
        rule.accept(this)
    })
    //Pop
    //println("xxxxx")
    while (scopeStack.size > 0) {
      val ntdef = scopeStack.pop()
      val maxSize = scopeMaxsize.get(ntdef.symbol).getOrElse(throw new RuntimeException("Cannot get max size for scope: "+ntdef.symbol))
      limitSize = maxSize
      keepPushingScope = false
      ntdef.accept(this)
    }
    //println("xasd")
  }

  override def visitNTDef(nTDef: NTDef) = {
    currentScope = nTDef.symbol
    sort = nTDef.sort
    if(keepPushingScope) {
      scopeStack.push(nTDef)
      if(!scopeMaxsize.contains(nTDef.symbol))
        scopeMaxsize += nTDef.symbol -> currentMaxSize
    }
    currentMaxSize = scopeMaxsize.get(nTDef.symbol).getOrElse(throw new RuntimeException("Found no currentmaxsize of this: "+nTDef))
    nTDef.expansions.foreach(e => e.accept(this))
  }

  override def visitFunGTerm(term: FunGTerm) ={
    if(term.args.size <= currentMaxSize - 1) {
      if(keepPushingScope) {
        term.args.foreach(arg => {
          if (arg.isInstanceOf[SymbolGTerm]) {
            val argSymbol = arg.asInstanceOf[SymbolGTerm].symbol
            if (isScopeSymbol(argSymbol)) {
              scopeMaxsize.get(argSymbol) match {
                case None => scopeMaxsize += (argSymbol -> (currentMaxSize - term.args.size))
                case Some(s) => {
                  if (s < currentMaxSize - term.args.size)
                    scopeMaxsize.update(argSymbol, currentMaxSize - term.args.size)
                }
              }
            }
          }
        })
      }else {
        //Do this while popping the scope stack
        val permittedSize = currentMaxSize - term.args.size
        val r = 1 to permittedSize
        r.foreach(size => {
          var sizeForFirstArg = true
          term.args.foreach(arg => {
            if (arg.isInstanceOf[SymbolGTerm]) {
              val argScope = arg.asInstanceOf[SymbolGTerm].symbol
              if (isScopeSymbol(argScope)) {
                expStack.get(argScope) match {
                  case None => {
                    throw new RuntimeException("Should never happen!")
                  }
                  case Some(expsOfSizes) => {
                    if (term.funName == "not") {
                      //Handle "not" differently since it is a unary operator
                      //r.foreach(size => {
                        expsOfSizes.get(size) match {
                          case None => {}
                          case Some(setExp) => {
                            setExp.foreach(e => {
                              if(e.isInstanceOf[BoolOperatorsCAST]) {
                                val newExp = ExtUtils.transformTheoryFun2CoreAST(term.funName, e, pos)
                                /*expsOfSizes.get(newExp.size()) match {
                                  case None => {
                                    val tempSet = new mutable.HashSet[CAST]()
                                    tempSet.add(newExp)
                                    expsOfSizes += newExp.size() -> tempSet
                                  }
                                  case Some(existingExps) => {
                                    existingExps.add(newExp)
                                  }
                                }
                                expStack.update(currentScope, expsOfSizes)*/
                                if(newExp != null) {
                                  expStack.get(currentScope) match {
                                    case None => {
                                      val mapCurrentScopeExps = new mutable.HashMap[Int, mutable.HashSet[CAST]]()
                                      val expsOfCurrentScope = new mutable.HashSet[CAST]()
                                      expsOfCurrentScope.add(newExp)
                                      mapCurrentScopeExps += newExp.size() -> expsOfCurrentScope
                                      expStack.update(currentScope,mapCurrentScopeExps)
                                    }
                                    case Some(e) => {
                                      e.get(newExp.size()) match {
                                        case None => {
                                          val tempSet = new mutable.HashSet[CAST]()
                                          tempSet.add(newExp)
                                          e += newExp.size() -> tempSet
                                        }
                                        case Some(existingExps) => {
                                          existingExps.add(newExp)
                                          e.update(newExp.size(), existingExps)
                                        }
                                      }
                                      expStack.update(currentScope, e)
                                    }
                                  }
                                }
                              }
                            })
                          }
                        }
                      //})
                    }
                    else {
                      //Handle binary operator
                      //r.foreach {
                      //  size => {
                          expsOfSizes.get(size) match {
                            case None => {
                              //We build exps of this "size" for current scope
                              /*if(currentScope.equals(argScope)){
                                val sizeRange = 1 to size
                                sizeRange.foreach(sr =>{
                                  expsOfSizes.get(sr) match {
                                    case None => {}
                                    case Some(expOfSR) =>{
                                      expsOfSizes.get(size - sr) match {
                                        case None => {}
                                        case Some(expOfRemain) => {
                                          val crossProduct = ExtUtils.crossProduct(List(expOfSR.toList, expOfRemain.toList))

                                        }
                                      }
                                    }
                                  }
                                })
                              }*/
                            }
                            case Some(e1) => {
                              //Important!!! see permittedSize - size + size = permittedSize. We try all combinations of this
                              val sizeForThisArg = if(sizeForFirstArg) size else permittedSize - size
                              expsOfSizes.get(sizeForThisArg) match {
                                case None => {}
                                case Some(e2) => {
                                  val crossP = ExtUtils.crossProduct(List(e1.toList, e2.toList))
                                  crossP.foreach(cr => {
                                    if(cr(0).getSort() == cr(1).getSort()) {
                                      val newExp = ExtUtils.transformTheoryFun2CoreAST(term.funName, cr(0), cr(1), this.pos)
                                      if(newExp != null) {
                                        expStack.get(currentScope) match {
                                          case None => {
                                            val mapCurrentScopeExps = new mutable.HashMap[Int, mutable.HashSet[CAST]]()
                                            val expsOfCurrentScope = new mutable.HashSet[CAST]()
                                            expsOfCurrentScope.add(newExp)
                                            mapCurrentScopeExps += newExp.size() -> expsOfCurrentScope
                                            expStack.update(currentScope,mapCurrentScopeExps)
                                          }
                                          case Some(e) => {
                                            e.get(newExp.size()) match {
                                              case None => {
                                                val tempSet = new mutable.HashSet[CAST]()
                                                tempSet.add(newExp)
                                                e += newExp.size() -> tempSet
                                              }
                                              case Some(existingExps) => {
                                                existingExps.add(newExp)
                                                e.update(newExp.size(), existingExps)
                                              }
                                            }
                                            expStack.update(currentScope, e)
                                          }
                                        }
                                      }
                                    }
                                  })
                                  //println("asdadsa")
                                }
                              }
                            }
                          }
                      // }
                      //}
                    }
                  }
                }
              } else {
                //TODO: handle function application on other terms later
              }
              sizeForFirstArg = false
            }
          })
        })//End of r for each to permitted
      }
    }
  }

  override def visitSymbolGTerm(term: SymbolGTerm) ={
    if(currentMaxSize >= 1 && !keepPushingScope) {
      val expSet = expStack.get(currentScope).getOrElse(new mutable.HashMap[Int, mutable.HashSet[CAST]]())
      sort match {
        case BoolSortExp() => {
          val s = expSet.get(1).getOrElse(new mutable.HashSet[CAST]())
          s.add(BoolVar(term.symbol))
          expSet += (1 -> s)
        }
        case IntSortExp() => {
          val s = expSet.get(1).getOrElse(new mutable.HashSet[CAST]())
          s.add(IntVar(term.symbol))
          expSet += (1 -> s)
        }
      }
      expStack.update(currentScope, expSet)
    }
  }

  override def visitLiteralGTerm(term: LiteralGTerm) ={
    if(currentMaxSize >= 1 && !keepPushingScope) {
      val expSet = expStack.get(currentScope).getOrElse(new mutable.HashMap[Int, mutable.HashSet[CAST]]())
      sort match {
        case BoolSortExp() => {
          val s = expSet.get(1).getOrElse(new mutable.HashSet[CAST]())
          s.add(BoolConst(term.litGTerm.litStr))
          expSet += (1 -> s)
        }
        case IntSortExp() => {
          val s = expSet.get(1).getOrElse(new mutable.HashSet[CAST]())
          s.add(IntConst(term.litGTerm.litStr))
          expSet += (1 -> s)
        }
      }
      expStack.update(currentScope, expSet)
    }
  }
}
