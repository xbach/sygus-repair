package engines;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import java.io.IOException;
import java.util.concurrent.*;


/**
 * Created by dxble on 6/12/16.
 */
public class ProcessExecutor {
    public Boolean getTimeout() {
        return isTimeout;
    }

    private Boolean isTimeout = false;

    public ArrayList<String> execute(List<String> command,int waitTime) {
        Process p = null;

        //System.out.println(fullPath);
        try {

            ProcessBuilder pb = new ProcessBuilder(command.toArray(new String[command.size()]));
            pb.redirectOutput();
            pb.redirectErrorStream(true);
            //long t_start = System.currentTimeMillis();
            p = pb.start();
            String cm2 = command.toString().replace("[", "").replace("]", "").replace(",", " ");
            //log.debug("Executing process: \n"+cm2);
            Worker worker = new Worker(p);
            worker.start();
            worker.join(waitTime);
            //long t_end = System.currentTimeMillis();
            if(!p.waitFor(waitTime, TimeUnit.SECONDS)){
                p.destroy();
                isTimeout = true;
                return null;
            }
            //worker.interrupt();
            // ---
            int exitvalue = p.exitValue();
            ArrayList<String> res = getTestResults(p);
            p.destroy();
            //log.debug("Execution time "+((t_end-t_start)/1000)+ " seconds");

            return res;
        } catch (IllegalArgumentException|IOException | InterruptedException ex) {
            System.out.println("The validation thread continues working " + ex.getMessage());
            if (p != null)
                p.destroy();
            throw new RuntimeException("Validation return null");
        }

    }

    public ArrayList<String> execute2BeautifyResult(String beautifierPath, String inputFile,int waitTime) {
        Process p = null;

        //System.out.println(fullPath);
        try {

            List<String> command = new ArrayList<String>();
            command.add(beautifierPath);
            command.add("-ipfile");
            command.add(inputFile);//JUnitTestExecutor.class.getName()

            ProcessBuilder pb = new ProcessBuilder(command.toArray(new String[command.size()]));
            pb.redirectOutput();
            pb.redirectErrorStream(true);
            //long t_start = System.currentTimeMillis();
            p = pb.start();
            String cm2 = command.toString().replace("[", "").replace("]", "").replace(",", " ");
            //log.debug("Executing process: \n"+cm2);
            Worker worker = new Worker(p);
            worker.start();
            worker.join(waitTime);
            //long t_end = System.currentTimeMillis();
            if(!p.waitFor(waitTime, TimeUnit.SECONDS)){
                p.destroy();
                isTimeout = true;
                return null;
            }
            //worker.interrupt();
            // ---
            int exitvalue = p.exitValue();
            ArrayList<String> res = getTestResults(p);
            p.destroy();
            //log.debug("Execution time "+((t_end-t_start)/1000)+ " seconds");

            return res;
        } catch (IllegalArgumentException|IOException | InterruptedException ex) {
            System.out.println("The validation thread continues working " + ex.getMessage());
            if (p != null)
                p.destroy();
            throw new RuntimeException("Validation return null");
        }

    }


    private ArrayList<String> getTestResults(Process p) {
        ArrayList<String> results = new ArrayList<>();
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            String out = "";
            //System.out.println("=====OUTPUTTEST====");
            while ((line = in.readLine()) != null) {
                results.add(line);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return results;
    }

    private static class Worker extends Thread {
        private final Process process;
        private Integer exit;

        private Worker(Process process) {
            this.process = process;
        }

        public void run() {
            try {
                exit = process.waitFor();
            } catch (InterruptedException ignore) {
                return;
            }
        }
    }
}
