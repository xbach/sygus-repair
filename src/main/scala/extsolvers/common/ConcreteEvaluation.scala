package extsolvers.common

import engines.coreast._

import scala.collection._
/**
  * Created by dxble on 7/13/16.
  */

/**
  * Evaluate based on concrete values of variables, and proposed (synthesized) solution of each function
  *
  * @param collectedSubst map of concrete value of each variable
  * @param funcSubs map of solution implementation of each function
  */
class ConcreteEvaluation(collectedSubst: DefaultSubsCollector, funcSubs: mutable.HashMap[FunctionSignature, Solution]) extends CASTVisitorBase{

  override def visitFunctionSignature(functionDesign: FunctionSignature): VisitorReturnType = {
    //val concrete = concreteSubs.get(functionDesign).getOrElse(null) //throw new RuntimeException("Found No Substitution of: "+functionDesign
    //if(concrete == null) {
      val subst = funcSubs.get(functionDesign).getOrElse(throw new RuntimeException("Found No Substitution of: " + functionDesign))
      return subst.accept(this)
    //}else
      //return concrete
  }

  override def visitBoolVar(boolVar: BoolVar): VisitorReturnType = {
    collectedSubst.findSubstitution(boolVar) match {
      case None => throw new RuntimeException("Found No Substitution of: "+boolVar)
      case Some(sub) => sub
    }
  }

  override def visitIntVar(intVar: IntVar): VisitorReturnType = {
    collectedSubst.findSubstitution(intVar) match {
      case None => {
        //collectedSubst.findSubstitution(intVar)
        throw new RuntimeException("Found No Substitution of: "+intVar)
      }
      case Some(sub) => sub
    }
  }
}
case class CollectAngelicValues() extends CASTVisitorBase {
  val funsAngelicValues: mutable.HashMap [FunctionSignature, mutable.HashSet[InternalTerm]] = new mutable.HashMap[FunctionSignature, mutable.HashSet[InternalTerm]]()
  var matchedFunSig: FunctionSignature = null

  override def visitFunctionSignature(functionDesign: FunctionSignature): VisitorReturnType = {
    matchedFunSig = functionDesign
    return EmptyType()
  }

  override def visitBoolConst(boolConst: BoolConst): VisitorReturnType={
    if(matchedFunSig != null){
      val angelicValues = funsAngelicValues.get(matchedFunSig).getOrElse(new mutable.HashSet[InternalTerm]())
      angelicValues.add(boolConst)
      funsAngelicValues.update(matchedFunSig, angelicValues)
      matchedFunSig = null
    }
    //super.visitBoolConst(boolConst)
    new BoolType(boolConst, boolConst.value.toBoolean)
  }

  override def visitIntConst(intConst: IntConst): VisitorReturnType={
    if(matchedFunSig != null){
      val angelicValues = funsAngelicValues.get(matchedFunSig).getOrElse(new mutable.HashSet[InternalTerm]())
      angelicValues.add(intConst)
      funsAngelicValues.update(matchedFunSig, angelicValues)
      matchedFunSig = null
    }
    //super.visitIntConst(intConst)
    new IntType(intConst, intConst.value.toInt)
  }

  override def visitBoolVar(boolVar: BoolVar): VisitorReturnType = ???

  override def visitIntVar(intVar: IntVar): VisitorReturnType = ???
}