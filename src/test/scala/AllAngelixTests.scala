import engines.RepairDriver

/**
  * Created by dxble on 6/23/16.
  */
//val param = "/home/dxble/Desktop/initRes/t14/last-angelic-forest.json /home/dxble/Desktop/initRes/t14/extracted patch /home/dxble/Desktop/initRes/t14/config.json Meta /home/dxble/workspace/synthesis/sygus-comp14/solvers/CVC4-0205-4/cvc4 /home/dxble/workspace/repairtools/angelix/src/af2sygus/sygusResult/parseResult.py"

object AllAngelixTests {
  val TEST_LOC_LAPTOP = "/home/dxble/workspace/repairtools/angelix/tests/"
  val HOME_DROPBOX_LAPTOP = "/home/dxble/Dropbox/"
  val TEST_LOC_DESKTOP = "/home/xuanbach/workspace/benchmarks/instrumented_IntroClass/median/h20/"
  val HOME_DROPBOX_DESKTOP = "/home/xuanbach/Dropbox/"
  val TEST_LOC_DROPBOX_LAPTOP = "/home/dxble/Dropbox/testing/"
  val TEST_AGL_EXP = "/home/dxble/workspace/angelix-experiments/"
  val INTRO_CLASS_LOC = "/home/dxble/workspace/introClass_instrument/IntroClassJava/dataset/smallest/36d8008b13f6475ca8fa4553fea10042b0a6c623665065672051445c3464d61b29b47cb66321844a0264505a0f5ccf5aa6de072aa266b5a8b0cf13198380a389/"

  val prjNames2 = List[String]("assignment", "enum", "guard", "if-condition"
    , "memberexpr", "multiline", "named-decl", "reachability", "reuse")

  val prjNames = List[String]("assignment", "deletion", "distance", "enum", "for-loop", "golden", "guard", "if-condition"
    , "loop-condition", "memberexpr", "multiline", "named-decl", "reachability", "semfix", "semfix-synthesis")

  /**
    * ../../tests/distance/.angelix/last-angelic-forest.json ../../tests/distance/.angelix/extracted/ ./myout ../../../../synthesis/mytest/config.json CVC4 /home/dxble/workspace/synthesis/sygus-comp14/solvers/CVC4-0205-4/cvc4 sygusResult/parseResult.py
    * ../../tests/distance/.angelix/last-angelic-forest.json ../../tests/distance/.angelix/extracted/ ./myout ../../../../synthesis/mytest/config.json Enum /home/dxble/workspace/synthesis/sygus-comp14/solvers/enumerative/esolver-synth-lib/bin/opt/esolver-synthlib sygusResult/parseResult.py
    * ../../tests/distance/.angelix/last-angelic-forest.json ../../tests/distance/.angelix/extracted/ ./myout ../../../../synthesis/mytest/config.json Symbolic /home/dxble/workspace/synthesis/sygus-comp14/solvers/symbolic/src/main.py sygusResult/parseResult.py
    * ../../tests/distance/.angelix/last-angelic-forest.json ../../tests/distance/.angelix/extracted/ ./myout ../../../../synthesis/mytest/config.json Stoc /home/dxble/workspace/repairtools/angelix/build/z3/build/stochpp_solver sygusResult/parseResult.py
    **
    */
  def testMetaSearchAll(): Unit ={
    // Note that we can't run all tests at the same time due to static variables used in the code,
    // e.g., see AF2Sygus.funSignatures.

    //testMetaSearch("enum", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testMetaSearch("guard", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testMetaSearch("if-condition", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testMetaSearch("memberexpr", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testMetaSearch("multiline", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testMetaSearch("named-decl", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testMetaSearch("reachability", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testMetaSearch("reuse", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testMetaSearch("median-h19-003", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testMetaSearch("median-h45-012", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
  }
  def main(args: Array[String]) {
    testMetaSearch2("003", INTRO_CLASS_LOC, "")
    //testMetaSearchAll()
    //prjNames2.foreach(prj => testMetaSearch(prj, TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP))

    //prjNames2.foreach(prj => testCVC4(prj, TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP))
    //testCVC4("reuse",TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testXStoc("trivialexp")
    //testCVC4("trivialexp", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testX("assignment")
    //testEnum("assignment", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testCVC4("median-h20-002", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testCVC4("", TEST_AGL_EXP, HOME_DROPBOX_LAPTOP)
    //testCVC4("digits-h18-000", "/home/dxble/Dropbox/testing/", HOME_DROPBOX_LAPTOP)
    //testCVC4("digits-h40-000", "/home/dxble/Dropbox/testing/", HOME_DROPBOX_LAPTOP)
    //testCVC4("median-h45-012", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testStoc("median-h45-012", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testEnum("median-h45-012", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testSymbolic("median-h19-003", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testEnum("median-h19-003", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testEnum("median-h44-003", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testEnum("median-h45-012", TEST_LOC_DROPBOX_LAPTOP, HOME_DROPBOX_LAPTOP)
    //To test median h44 003
    //testEnum("median-h19-003", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
    //testMetaSearch("median-h19-003", TEST_LOC_LAPTOP, HOME_DROPBOX_LAPTOP)
  }

  def testMetaSearch2(prjName: String, testLoc: String, homeDropbox: String): Unit = {
    println("Running: "+prjName)
    val args = testLoc+prjName+"/angelix/last-angelic-forest.json "+testLoc+prjName+"/angelix/extracted/ ./myout_"+normProjectName(prjName)+" examples/config.json Meta "+homeDropbox+"/CVC4-0205-4/cvc4 sygusResult/parseResult.py"
    RepairDriver.main(args.split(" "))
    println("Finished: "+prjName)
  }

  def testMetaSearch(prjName: String, testLoc: String, homeDropbox: String): Unit = {
    println("Running: "+prjName)
    val args = testLoc+prjName+"/.angelix/last-angelic-forest.json "+testLoc+prjName+"/.angelix/extracted/ ./myout_"+normProjectName(prjName)+" examples/config.json Meta "+homeDropbox+"/CVC4-0205-4/cvc4 sygusResult/parseResult.py"
    RepairDriver.main(args.split(" "))
    println("Finished: "+prjName)
  }

  def testCVC4(prjName: String, testLoc: String, homeDropbox: String): Unit = {
    println("Running: "+prjName)
    val args = testLoc+prjName+"/.angelix/last-angelic-forest.json "+testLoc+prjName+"/.angelix/extracted/ ./myout_"+normProjectName(prjName)+" examples/config.json CVC4 "+homeDropbox+"/CVC4-0205-4/cvc4 sygusResult/parseResult.py"
    RepairDriver.main(args.split(" "))
    println("Finished: "+prjName)
  }

  def testStoc(prjName: String, testLoc: String, homeDropbox: String): Unit = {
    println("Running: "+prjName)
    val args = testLoc+prjName+"/.angelix/last-angelic-forest.json "+testLoc+prjName+"/.angelix/extracted/ ./myout_"+normProjectName(prjName)+" examples/config.json Stoc "+homeDropbox+"/stochpp_solver sygusResult/parseResult.py"
    RepairDriver.main(args.split(" "))
  }

  def testEnum(prjName: String, testLoc: String, homeDropbox: String): Unit = {
    println("Running: "+prjName)
    val args = testLoc+prjName+"/.angelix/last-angelic-forest.json "+testLoc+prjName+"/.angelix/extracted/ ./myout_"+normProjectName(prjName)+" examples/config.json Enum "+homeDropbox+"/esolver-synthlib sygusResult/parseResult.py"
    RepairDriver.main(args.split(" "))
  }

  def testSymbolic(prjName: String, testLoc: String, homeDropbox: String): Unit = {
    println("Running: "+prjName)
    val args = testLoc+prjName+"/.angelix/last-angelic-forest.json "+testLoc+prjName+"/.angelix/extracted/ ./myout_"+normProjectName(prjName)+" examples/config.json Enum "+homeDropbox+"/symbolic/src/main.py sygusResult/parseResult.py"
    RepairDriver.main(args.split(" "))
  }

  private def normProjectName(prjName: String): String ={
    if (prjName == "")
      return prjName

    return prjName.replace("/","_")
  }
}
