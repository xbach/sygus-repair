package engines

import java.util

import common.Utils
import engines.coreast._
import engines.features.RankingFeature
import extsolvers.metasearch.MetaSearchSolver

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

/**
  * Created by dxble on 6/12/16.
  */
abstract class Solution{

}
case class TrivialSolution(sol: String) extends Solution{

}
case class OKSolution(sol: String) extends Solution{

}
case class NotFoundSolution() extends Solution{

}

abstract class Synthesizers {
  private var sygusFile = ""
  private var solverPath = ""
  private var resultBeautifierPath = ""
  private var extractedDir = ""
  private var rand: Random = new Random(0)
  private var features: ArrayBuffer[RankingFeature] = new ArrayBuffer[RankingFeature]()

  def getRandom(): Random = rand
  def setRandom(r: Random) = rand = r

  def addFeature(feature: RankingFeature) = {
    if(features == null)
      features = new ArrayBuffer[RankingFeature]()
    features.append(feature)
  }

  def getFeatures() = features

  //return results, timeout, success
  def solve(): (Solution, Boolean, Boolean) ={
    val executor = new ProcessExecutor
    val command = solverCommand()
    val res = executor.execute(command, getConfig().solverTimeout)
    if(executor.getTimeout){
      return (null,true,false)
    }else{
      if(hasNoSolutions(res))
        return (null,false,false)
      else {
        val solutions = getSolutions(res, getConfig())
        /** TODO:
          * for now just take the first solution appearing in the solutions list
          * we can do traversing and choose the one with fewer constants to force generalization
          */
        val bestSol = chooseBestSolution(solutions)
        if (Utils.enableLogging)
         println(bestSol)
        return (bestSol, false, true)
      //return (res,false,true)
      }
    }
  }

  def getConfig(): SynthesisConfig

  def solverCommand(): util.ArrayList[String]

  def setSyGusFile(f: String) ={
    sygusFile = f
  }
  def getSyGusFile(): String ={
    return sygusFile
  }
  def setSolverPath(p: String) ={
    solverPath = p
  }

  def getSolverPath(): String ={
    return solverPath
  }

  def setBeautifierPath(p: String) ={
    resultBeautifierPath = p
  }

  def getBeautifierPath(): String ={
    return resultBeautifierPath
  }

  def hasNoSolutions(res: util.ArrayList[String]): Boolean ={
    import scala.collection.JavaConversions._
    //CVC4 returns unknown, Enum returns No Solutions
    //return res.exists(e => e.contains("No Solutions") || e.contains("unknown") || e.contains("Error:") || e.contains("Exception"))
    return !res.exists(e => e.contains("define-fun"))
  }

  def beautifierCommand(input: String): util.ArrayList[String] ={
    val command = new util.ArrayList[String]()
    command.add("python")
    command.add(getBeautifierPath())
    command.add("-ipfile")
    command.add(input)
    command.add("-log")
    command.add("-solver")
    //Remember that we overide this beautifier command function for stochastic synthesizer
    command.add(getSolverName())
    if(getConfig().simplification) {
      command.add("-simplify")
      command.add("-extracted")
      command.add(getExtractedDir())
    }
    return command
  }

  def getExtractedDir():String={
    return extractedDir
  }

  def setExtractedDir(dir: String)={
    extractedDir = dir
  }

  def getSolverName(): String

  def executeBeautifier(config: SynthesisConfig, input: String): util.ArrayList[String] ={
    val executor = new ProcessExecutor
    val command = beautifierCommand(input)
    return executor.execute(command,config.solverTimeout)
  }
  //solution id -> solution (define-fun ...)
  //Currently this is implemented for Enum
  def getSolutions(rawOutput: util.ArrayList[String], config: SynthesisConfig): util.ArrayList[String] ={
    //val solutions = new ArrayBuffer[String]()
    var eachSol = ""
    val res = new util.ArrayList[String]()
    var i = 0
    var encounteredSol = false
    while (i<rawOutput.size()){
      val line = rawOutput.get(i)
      if(!encounteredSol && line.contains("Solution"))
        encounteredSol = true
      if(!line.contains("Solution") && line != "" && !line.contains("----") && encounteredSol){
        eachSol += line +"\n"
      }else if(line.contains("----")){
        //Output solutions to a file so that the result beautifier can read and return a result
        val tempOutput = getSyGusFile()+".output"
        Utils.writeToFile(tempOutput, eachSol)
        import scala.collection.JavaConversions._
        val execRes = executeBeautifier(config, tempOutput)
        val resStr = execRes.foldLeft(""){ (res, r) =>{ res+r+"\n"}}
        res.append(resStr)
        //restore eachSol to empty after processing each solution is done
        eachSol = ""
      }
      i+=1
    }
    return res
  }

  /**
    * TODO: need a strategy to choose best solution, now only take the first solution in the list
    *
    * @param solutions
    * @return
    */
  def chooseBestSolution(solutions: util.ArrayList[String]): Solution={
    if(!getConfig().simplification)
      return new OKSolution(solutions.get(0))
    else {
      /**
        * simplification will do: not choose trivial result, for now trivial result is constant expression
        */
      import scala.collection.JavaConversions._
      val notTrivialSolutions = solutions.filter(p => !p.contains("Trivial"))
      if (notTrivialSolutions != null && notTrivialSolutions.size >= 1){
        //for now return the first not trivial solution
        return OKSolution(notTrivialSolutions.get(0))
      }else{
        return TrivialSolution(solutions.get(0))
      }
    }
  }
}
case class EnumerativeSynth(config: SynthesisConfig) extends Synthesizers {
  override def getConfig(): SynthesisConfig = return config
  override def getSolverName(): String = "Enum"
  override def solverCommand(): util.ArrayList[String] ={
    val command = new util.ArrayList[String]()
    command.add(getSolverPath())
    command.add(getSyGusFile())
    command.add("-s")
    command.add("30")
    return command
  }
}
case class SymbolicSynth(config: SynthesisConfig) extends Synthesizers{
  override def getConfig(): SynthesisConfig = return config
  override def getSolverName(): String = "Symbolic"
  override def solverCommand(): util.ArrayList[String] ={
    val command = new util.ArrayList[String]()
    command.add("python")
    command.add(getSolverPath())
    command.add("-file")
    command.add(getSyGusFile())
    return command
  }

  //solution id -> solution (define-fun ...)
  override def getSolutions(rawOutput: util.ArrayList[String], config: SynthesisConfig): util.ArrayList[String] ={
    //val solutions = new ArrayBuffer[String]()
    var eachSol = ""
    val res = new util.ArrayList[String]()
    var i = 0
    while (i<rawOutput.size()){
      val line = rawOutput.get(i)
      eachSol += line +"\n"
      i+=1
    }
    val tempOutput = getSyGusFile()+".output"
    Utils.writeToFile(tempOutput, eachSol)
    val execRes = executeBeautifier(config, tempOutput)
    import scala.collection.JavaConversions._
    val resStr = execRes.foldLeft(""){ (res, r) =>{ res+r+"\n"}}
    res.append(resStr)
    return res
  }
}
case class CVC4Synth(config: SynthesisConfig) extends Synthesizers{
  override def getConfig(): SynthesisConfig = return config
  override def getSolverName(): String = "CVC4"
  override def solverCommand(): util.ArrayList[String] ={
    val command = new util.ArrayList[String]()
    command.add(getSolverPath())
    command.add("--cegqi")
    command.add("--lang")
    command.add("sygus")
    command.add("--dump-synth")
    command.add(getSyGusFile())
    return command
  }

  //solution id -> solution (define-fun ...)
  override def getSolutions(rawOutput: util.ArrayList[String], config: SynthesisConfig): util.ArrayList[String] ={
    //val solutions = new ArrayBuffer[String]()
    var eachSol = ""
    val res = new util.ArrayList[String]()
    var i = 0
    while (i<rawOutput.size()){
      val line = rawOutput.get(i)
      if(line != "Solution:" && line != "unsat")
      eachSol += line +"\n"
      i+=1
    }
    val tempOutput = getSyGusFile()+".output"
    Utils.writeToFile(tempOutput, eachSol)
    val execRes = executeBeautifier(config, tempOutput)
    import scala.collection.JavaConversions._
    val resStr = execRes.foldLeft(""){ (res, r) =>{ res+r+"\n"}}
    res.append(resStr)
    return res
  }
}
case class StochasticSynth(config: SynthesisConfig) extends Synthesizers{
  override def getConfig(): SynthesisConfig = return config
  override def getSolverName(): String = "Stoc"
  override def solverCommand(): util.ArrayList[String] ={
    val command = new util.ArrayList[String]()
    command.add(getSolverPath())
    command.add(getSyGusFile())
    return command
  }

  //solution id -> solution (define-fun ...)
  override def getSolutions(rawOutput: util.ArrayList[String], config: SynthesisConfig): util.ArrayList[String] ={
    //val solutions = new ArrayBuffer[String]()
    var eachSol = ""
    val res = new util.ArrayList[String]()
    var i = 0
    while (i<rawOutput.size()){
      val line = rawOutput.get(i)
      if(line != "Solution:" && line != "unsat")
        eachSol += line +"\n"
      i+=1
    }
    val tempOutput = getSyGusFile()+".output"
    Utils.writeToFile(tempOutput, eachSol)
    val execRes = executeBeautifier(config, tempOutput)
    import scala.collection.JavaConversions._
    val resStr = execRes.foldLeft(""){ (res, r) =>{ res+r+"\n"}}
    res.append(resStr)
    return res
  }

  override def beautifierCommand(input: String): util.ArrayList[String] ={
    val command = new util.ArrayList[String]()
    command.add("python")
    command.add(getBeautifierPath())
    command.add("-ipfile")
    command.add(input)
    command.add("-log")
    command.add("-solver")
    command.add("Stoc")
    command.add("-sygusfile")
    command.add(getSyGusFile())
    if(getConfig().simplification) {
      command.add("-simplify")
      command.add("-extracted")
      command.add(getExtractedDir())
    }
    return command
  }
}
case class MetaSearchSynth(config: SynthesisConfig) extends Synthesizers{
  override def getConfig(): SynthesisConfig = return config
  override def getSolverName(): String = "Meta"

  override def solve(): (Solution, Boolean, Boolean) ={
    val solution = MetaSearchSolver.solveFromFile(getSyGusFile(), getRandom(), getFeatures(), config)
    if(solution != null) {
      val solStr = solution.foldLeft("") {
        (resStr, eachFun) => {
          resStr + eachFun._1.funName + ":" + simplifyExp(eachFun._2.getTransformedCAST()) + "\n"
        }
      }
      return (OKSolution(solStr), false, true)
    }else{
      return (NotFoundSolution(), false, true)
    }
  }

  override def solverCommand(): util.ArrayList[String] ={null}

  private def simplifyExp(exp: CAST): CAST = {
    exp match {
      case And(left, right) => {
        if(left.isInstanceOf[BoolConst]){
          if(left.asInstanceOf[BoolConst].value.compareTo("false") == 0)
            left
          else simplifyExp(right)
        }else if(right.isInstanceOf[BoolConst]){
          if(right.asInstanceOf[BoolConst].value.compareTo("false") == 0)
            right
          else simplifyExp(left)
        }
        else And(simplifyExp(left).asInstanceOf[BoolOperatorsCAST], simplifyExp(right).asInstanceOf[BoolOperatorsCAST])
      }
      case Or(left, right) => {
        if(left.isInstanceOf[BoolConst]){
          if(left.asInstanceOf[BoolConst].value.compareTo("true") == 0)
            left
          else simplifyExp(right)
        }else if(right.isInstanceOf[BoolConst]){
          if(right.asInstanceOf[BoolConst].value.compareTo("true") == 0)
            right
          else simplifyExp(left)
        }
        else Or(simplifyExp(left).asInstanceOf[BoolOperatorsCAST], simplifyExp(right).asInstanceOf[BoolOperatorsCAST])
      }
      case e: CAST => e
    }
  }
}
