package synthlib2parser

/**
  * Created by dxble on 7/8/16.
  */

import scala.util.parsing.combinator.RegexParsers

object Synthlib2Lexer extends RegexParsers {
  override def skipWhitespace = true
  override val whiteSpace = """([\s\t\r\f]|;.*)+""".r //"[ \t\r\f]+".r

  def apply(code: String): Either[Synthlib2LexerError, List[Synthlib2Token]] = {
    parse(tokens, code) match {
      case NoSuccess(msg, next) => Left(Synthlib2LexerError(Location(next.pos.line, next.pos.column), msg))
      case Success(result, next) => Right(result)
    }
  }

  def tokens: Parser[List[Synthlib2Token]] = {
    phrase(rep1( setlogic | leftparent | rightparent
      |definesort
      |definefun
      |declarefun
      |checksynth
      |declarevar
      |synthfun
      |constraint
      |assert
      |originalexpr
      |bitvec
      |array
      |inttk
      |booltk
      |enumtk
      |realtk
      |constanttk
      |inputvartk
      |localvartk
      |variabletk
      |lettk
      |doublecoltk
      |truetk
      |falsetk
      |intconst
      |symbol
      /*|exit
      | readInput | callService | switch | otherwise | colon | arrow
      | equals | comma | literal | identifier | indentation*/)) ^^ { rawTokens =>
      //processIndentations(rawTokens)
      rawTokens
    }
  }

  private def processIndentations(tokens: List[Synthlib2Token],
                                  indents: List[Int] = List(0)): List[Synthlib2Token] = {
    tokens.headOption match {

      // if there is an increase in indentation level, we push this new level into the stack
      // and produce an INDENT
      case Some(INDENTATION(spaces)) if spaces > indents.head =>
        INDENT() :: processIndentations(tokens.tail, spaces :: indents)

      // if there is a decrease, we pop from the stack until we have matched the new level and
      // we produce a DEDENT for each pop
      case Some(INDENTATION(spaces)) if spaces < indents.head =>
        val (dropped, kept) = indents.partition(_ > spaces)
        (dropped map (_ => DEDENT())) ::: processIndentations(tokens.tail, kept)

      // if the indentation level stays unchanged, no tokens are produced
      case Some(INDENTATION(spaces)) if spaces == indents.head =>
        processIndentations(tokens.tail, indents)

      // other tokens are ignored
      case Some(token) =>
        token :: processIndentations(tokens.tail, indents)

      // the final step is to produce a DEDENT for each indentation level still remaining, thus
      // "closing" the remaining open INDENTS
      case None =>
        indents.filter(_ > 0).map(_ => DEDENT())

    }
  }

  /*def identifier: Parser[IDENTIFIER] = positioned {
    "[a-zA-Z_][a-zA-Z0-9_]*".r ^^ { str => IDENTIFIER(str) }
  }

  def literal: Parser[LITERAL] = positioned {
    """"[^"]*"""".r ^^ { str =>
      val content = str.substring(1, str.length - 1)
      LITERAL(content)
    }
  }*/

  def indentation: Parser[INDENTATION] = positioned {
    "\n[ ]*".r ^^ { whitespace =>
      val nSpaces = whitespace.length - 1
      INDENTATION(nSpaces)
    }
  }

  def digit: Parser[DIGIT] = positioned {
    "-?[0-9]".r ^^ { str => DIGIT(str) }
  }

  def intconst: Parser[TK_INT_LITERAL] = positioned {
    "-?[0-9]+".r ^^ { str => TK_INT_LITERAL(str) }
  }

  def symbol: Parser[TK_SYMBOL] = positioned {
    //"abcd".r ^^ { str => TK_SYMBOL(str) }
    val symbolcc = "([a-z]|[A-Z]|_|\\+|-|\\*|&|\\|\\!|\\~|\\<|\\>|\\=|\\/|\\%|\\?|\\.|\\$|\\^)([a-z]|[A-Z]|_|\\+|-|\\*|&|\\|\\!|\\~|\\<|\\>|\\=|\\/|\\%|\\?|\\.|\\$|\\^|[0-9])*".r ^^ {case str => TK_SYMBOL(str)}
    symbolcc
    //symbolcc //| rep(symbolcc|digit)
  }

  //-----
  def setlogic      = positioned {"set-logic"      ^^ (_ => TK_SETLOGIC())}
  def leftparent      = positioned {"("      ^^ (_ => TK_LPARENT())}
  def rightparent      = positioned {")"      ^^ (_ => TK_RPARENT())}
  def definesort = positioned  {"define-sort" ^^ (_ => TK_DEFINE_SORT()) }
  def definefun = positioned {"define-fun"  ^^   (_ => TK_DEFINE_FUN()) }
  def declarefun = positioned {"declare-fun"  ^^   (_ => TK_DECLARE_FUN()) }
  def checksynth = positioned {"check-synth"  ^^   (_ => TK_CHECK_SYNTH()) }
  def declarevar = positioned {"declare-var"  ^^   (_ => TK_DECLARE_VAR()) }
  def synthfun = positioned {"synth-fun"  ^^   (_ => TK_SYNTH_FUN()) }
  def constraint = positioned {"constraint"  ^^   (_ => TK_CONSTRAINT()) }
  def assert = positioned {"assert"  ^^   (_ => TK_ASSERT()) }
  def originalexpr = positioned {"original-expr"  ^^   (_ => TK_ORIGINALEXPR()) }
  def bitvec = positioned {"BitVec" ~not(symbol)  ^^   (_ => TK_BV()) }
  def array = positioned {"Array" ~not(symbol)  ^^   (_ => TK_ARRAY()) }
  def inttk = positioned {"Int" ~not(symbol)  ^^   (_ => TK_INT()) }
  def booltk = positioned {"Bool" ~not(symbol)  ^^   (_ => TK_BOOL()) }
  def enumtk = positioned {"Enum" ~not(symbol)  ^^   (_ => TK_ENUM()) }
  def realtk = positioned {"Real" ~not(symbol)  ^^   (_ => TK_REAL()) }
  def constanttk = positioned {"Constant"  ^^   (_ => TK_CONSTANT()) }
  def variabletk = positioned {"Variable"~not(symbol)  ^^   (_ => TK_VARIABLE()) }
  def inputvartk = positioned {"InputVariable"  ^^   (_ => TK_INPUT_VARIABLE()) }
  def localvartk = positioned {"LocalVariable"  ^^   (_ => TK_LOCAL_VARIABLE()) }
  def lettk = positioned {"let"  ^^   (_ => TK_LET()) }
  def doublecoltk = positioned {"::"  ^^   (_ => TK_DOUBLECOLON()) }
  def truetk = positioned {"true"  ^^   (_ => TK_BOOL_LITERAL("true")) }
  def falsetk = positioned {"false"  ^^   (_ => TK_BOOL_LITERAL("false")) }
  //def enumtk = positioned {"Enum"  ^^   (_ => TK_ENUM()) }

}