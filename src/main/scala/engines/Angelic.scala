package engines

import java.io.File

import argonaut._
import Argonaut._
import argonaut.EncodeJson

import AngelicFix._

import java.io.File
import java.util.Calendar
import java.text.SimpleDateFormat
import argonaut._
import Argonaut._
import argonaut.EncodeJson
//import edu.nus.mrepair.synthesis._
//import edu.nus.mrepair.decision.SolverStat
//import edu.nus.mrepair.SynthesisConfig
//import edu.nus.mrepair.BenchmarkExecutionResult
//import edu.nus.mrepair.SuspiciousLocations

object Report {

  implicit def EncodingLevelDecodeJson: DecodeJson[EncodingLevel] =
    DecodeJson(c => for {
      str <- c.as[String]
    } yield {
      str match {
        case "linear" => Linear()
        case "logical" => Logical()
        case "nonlinear" => NonLinear()
      }
    })

  implicit def EncodingLevelEncodeJson: EncodeJson[EncodingLevel] =
    EncodeJson({
      case Linear() => jString("linear")
      case Logical() => jString("logical")
      case NonLinear() => jString("nonlinear")
    })

  implicit def ComponentLevelDecodeJson: DecodeJson[ComponentLevel] =
    DecodeJson(c => for {
      str <- c.as[String]
    } yield {
      str match {
        case "alternatives"           => Alternatives()
        case "integer-constants"      => IntegerConstants()
        case "boolean-constants"      => BooleanConstants()
        case "variables"              => Variables()
        case "basic-arithmetic"       => BasicArithmetic()
        case "basic-logic"            => BasicLogic()
        case "basic-inequalities"     => BasicInequalities()
        case "extended-arithmetic"    => ExtendedArithmetic()
        case "extended-logic"         => ExtendedLogic()
        case "extended-inequalities"  => ExtendedInequalities()
        case "mixed-conditional"      => MixedConditional()
        case "conditional-arithmetic" => ConditionalArithmetic()
      }
    })

  implicit def ComponentLevelEncodeJson: EncodeJson[ComponentLevel] =
    EncodeJson({
      case Alternatives()          => jString("alternatives")
      case IntegerConstants()      => jString("integer-constants")
      case BooleanConstants()      => jString("boolean-constants")
      case Variables()             => jString("variables")
      case BasicArithmetic()       => jString("basic-arithmetic")
      case BasicLogic()            => jString("basic-logic")
      case BasicInequalities()     => jString("basic-inequalities")
      case ExtendedArithmetic()    => jString("extended-arithmetic")
      case ExtendedLogic()         => jString("extended-logic")
      case ExtendedInequalities()  => jString("extended-inequalities")
      case MixedConditional()      => jString("mixed-conditional")
      case ConditionalArithmetic() => jString("conditional-arithmetic")
    })

  /*implicit def SuspiciousLocationsCodecJson : CodecJson[SuspiciousLocations] =
    casecodec2(SuspiciousLocations.apply,
      SuspiciousLocations.unapply)("functions", "globalVariables")

  implicit def SolverStatCodecJson : CodecJson[SolverStat] =
    casecodec3(SolverStat.apply,
      SolverStat.unapply)("unsatCores", "soft", "hard")*/

  implicit def EncodingConfigCodecJson : CodecJson[EncodingConfig] =
    casecodec5(EncodingConfig.apply,
      EncodingConfig.unapply)("level",
      "repairIntegerConst",
      "repairBooleanConst",
      "componentsMultipleOccurrences",
      "phantomComponents")

  implicit def SynthesisConfigCodecJson : CodecJson[SynthesisConfig] =
    casecodec7(SynthesisConfig.apply,
      SynthesisConfig.unapply)("encodingConfig",
      "simplification",
      "reuseStructure",
      "spaceReduction",
      "solverBound",
      "solverTimeout",
      "componentLevel")

  /*implicit def BenchmarkExecutionResultCodecJson:
  CodecJson[BenchmarkExecutionResult] =
    casecodec16(BenchmarkExecutionResult.apply,
      BenchmarkExecutionResult.unapply)("benchmark",
      "version",
      "config",
      "vcProcessingTime",
      "tfProcessingTime",
      "rcProcessingTime",
      "rcSimplificationTime",
      "rcSolvingTime",
      "fixed",
      "isTimeout",
      "verified",
      "patch",
      "date",
      "localization",
      "locations",
      "solverStat")

  def toString(report: BenchmarkExecutionResult): String = {
    report.asJson.spaces2
  }*/

  def parseConfig(file: File): SynthesisConfig = {
    val configSource = scala.io.Source.fromFile(file)
    val str = configSource.mkString
    val Some(config) = str.decodeOption[SynthesisConfig]
    config
  }

}

object AFParser {

  implicit def VariableValueDecodeJson: DecodeJson[VariableValue] =
    DecodeJson(c => for {
      name <- (c --\ "name").as[String]
      value <- (c --\ "value").as[Json]
    } yield {
      if (value.isBool) {
        BoolVal(name, value.bool.get)
      } else {
        IntVal(name, value.number.get.toInt.get)
      }
    })

  implicit def VariableValueEncodeJson: EncodeJson[VariableValue] =
    EncodeJson({
      case IntVal(name, i) =>
        ("name" := name) ->:
          ("value" := i) ->: jEmptyObject
      case BoolVal(name, b) =>
        ("name" := name) ->:
          ("value" := b) ->: jEmptyObject
    })

  implicit def AngelicValueCodecJson : CodecJson[AngelicValue] =
    casecodec4(AngelicValue.apply,
      AngelicValue.unapply)("context", "value", "expression", "instId")


  def parse(file: File): AngelicForest = {
    val afSource = scala.io.Source.fromFile(file)
    val str = afSource.mkString
    val Some(af) = str.decodeOption[AngelicForest]
    af
  }

}

case class SynthesisConfig(synthesisConfig: EncodingConfig,
                           simplification: Boolean,
                           reuseStructure: Boolean,
                           spaceReduction: Boolean,
                           solverBound: Int,
                           solverTimeout: Int,
                           componentLevel: ComponentLevel)

sealed trait ComponentLevel
case class Alternatives() extends ComponentLevel
case class IntegerConstants() extends ComponentLevel
case class BooleanConstants() extends ComponentLevel
case class Variables() extends ComponentLevel
case class BasicArithmetic() extends ComponentLevel
case class BasicLogic() extends ComponentLevel
case class BasicInequalities() extends ComponentLevel
case class ExtendedArithmetic() extends ComponentLevel
case class ExtendedLogic() extends ComponentLevel
case class ExtendedInequalities() extends ComponentLevel
case class MixedConditional() extends ComponentLevel
case class ConditionalArithmetic() extends ComponentLevel

sealed trait EncodingLevel
case class Logical() extends EncodingLevel
case class Linear() extends EncodingLevel
case class NonLinear() extends EncodingLevel

case class EncodingConfig(level: EncodingLevel,
                          repairIntegerConst: Boolean,
                          repairBooleanConst: Boolean,
                          componentsMultipleOccurrences: Boolean,
                          phantomComponents: Boolean)

object AngelicFix {

  sealed trait VariableValue

  case class IntVal(name: String, value: Int) extends VariableValue

  case class BoolVal(name: String, value: Boolean) extends VariableValue

  case class CharVal(name: String, value: Char) extends VariableValue

  def renameVal(v: VariableValue, newname: String): VariableValue = {
    v match {
      case IntVal(n, v) => IntVal(newname, v)
      case BoolVal(n, v) => BoolVal(newname, v)
      case CharVal(n, v) => CharVal(newname, v)
    }
  }

  def getName(v: VariableValue): String = {
    v match {
      case IntVal(n, v) => n
      case BoolVal(n, v) => n
      case CharVal(n, v) => n
    }
  }

  case class AngelicValue(context: List[VariableValue], value: VariableValue, expression: String, instId: Int)

  type AngelicPath = List[AngelicValue]

  type AngelicForest = Map[String, List[AngelicPath]]
}