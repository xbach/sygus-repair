package engines

import java.io.File
import java.util.Random

import common.Utils
import config.ConfigurationPropertiesX
//import edu.nus.mrepair.{Driver, Report, Utils}
import org.apache.log4j.{BasicConfigurator, Logger}

/**
  ../../tests/distance/.angelix/last-angelic-forest.json ../../tests/distance/.angelix/extracted/ ./myout ../../../../synthesis/mytest/config.json CVC4 /home/dxble/workspace/synthesis/sygus-comp14/solvers/CVC4-0205-4/cvc4 sygusResult/parseResult.py
  ../../tests/distance/.angelix/last-angelic-forest.json ../../tests/distance/.angelix/extracted/ ./myout ../../../../synthesis/mytest/config.json Enum /home/dxble/workspace/synthesis/sygus-comp14/solvers/enumerative/esolver-synth-lib/bin/opt/esolver-synthlib sygusResult/parseResult.py
  ../../tests/distance/.angelix/last-angelic-forest.json ../../tests/distance/.angelix/extracted/ ./myout ../../../../synthesis/mytest/config.json Symbolic /home/dxble/workspace/synthesis/sygus-comp14/solvers/symbolic/src/main.py sygusResult/parseResult.py
  ../../tests/distance/.angelix/last-angelic-forest.json ../../tests/distance/.angelix/extracted/ ./myout ../../../../synthesis/mytest/config.json Stoc /home/dxble/workspace/repairtools/angelix/build/z3/build/stochpp_solver sygusResult/parseResult.py

  /home/dxble/workspace/angelix-experiments/.angelix/last-angelic-forest.json  /home/dxble/workspace/angelix-experiments/.angelix/extracted ./myoutput /home/dxble/workspace/synthesis/mytest/config.json Enum /home/dxble/workspace/synthesis/sygus-comp14/solvers/enumerative/esolver-synth-lib/bin/opt/esolver-synthlib /home/dxble/workspace/repairtools/angelix/src/af2sygus/sygusResult/parseResult.py
  */
object RepairDriver{
  val logger = Logger.getLogger(this.getClass)
  var result = false

  case class ProgramOptions(benchmark: String = "",
                            version: String = "",
                            logging: Boolean = false,
                            localization: Boolean = false,
                            quiet: Boolean = false,
                            config: File = new File("default.json"))

  def isWindows = System.getProperty("os.name").startsWith("Windows")

  def main(args: Array[String]) {
    //Driver
    /*if (isWindows) {
      System.loadLibrary("libz3")
      System.loadLibrary("libz3java")
    }*/

    // for Angelix:

    Utils.enableLogging = false
    //Driver
    // Usage:
    // synthesis <angelic-forest-file> <extacted-dir> <output-patch-file> <config-file> <level>+
    //   exitcode: 0 unless error
    //   stdout: SUCCESS, FAIL, TIMEOUT
    //   stderr: logging

    val angelicForestFile = args(0)
    val extractedDir=args(1)
    val outputFile=args(2)
    val configFile=args(3)
    val solver=args(4)
    val solverPath=args(5)
    val beautifierPath=args(6)
    BasicConfigurator.configure()
    val start = System.currentTimeMillis()
    val config = Report.parseConfig(new File(configFile))
    //val angelicForest = AFRepair.generateAngelicForest(smtFiles, testUniverseDir, testSuiteIds)
    val angelicForest = AFParser.parse(new File(angelicForestFile))
    val rand = new Random(config.solverTimeout)//

    if(solver.equals("Enum")) {
      val solver = new EnumerativeSynth(config)
      solver.setSolverPath(solverPath)
      solver.setBeautifierPath(beautifierPath)
      solver.setExtractedDir(extractedDir)
      solver.setRandom(rand)
      RepairSynthesis.generatePatch(extractedDir, angelicForest, solver, outputFile, config)
    }
    else if(solver.equals("Symbolic")) {
      val solver = new SymbolicSynth(config)
      solver.setSolverPath(solverPath)
      solver.setBeautifierPath(beautifierPath)
      solver.setExtractedDir(extractedDir)
      solver.setRandom(rand)
      RepairSynthesis.generatePatch(extractedDir, angelicForest, solver, outputFile, config)
    }
    else if(solver.equals("CVC4")) {
      val solver = new CVC4Synth(config)
      solver.setSolverPath(solverPath)
      solver.setBeautifierPath(beautifierPath)
      solver.setExtractedDir(extractedDir)
      solver.setRandom(rand)
      RepairSynthesis.generatePatch(extractedDir, angelicForest, solver, outputFile, config)
    }
    else if(solver.equals("Stoc")) {
      val solver = new StochasticSynth(config)
      solver.setSolverPath(solverPath)
      solver.setBeautifierPath(beautifierPath)
      solver.setExtractedDir(extractedDir)
      solver.setRandom(rand)
      RepairSynthesis.generatePatch(extractedDir, angelicForest, solver, outputFile, config)
    }
    else if(solver.equals("Meta")){
      val additionalConfig = args(7)
      ConfigurationPropertiesX.load(additionalConfig)
      assert(ConfigurationPropertiesX.properties != null)
      val solver = new MetaSearchSynth(config)
      solver.setRandom(rand)
      RepairSynthesis.generatePatch(extractedDir, angelicForest, solver, outputFile, config)
    }
    else
      throw new RuntimeException("!!! Not Supported Solver: "+solver)

    result = RepairSynthesis.synthesisSuccess

    val end = System.currentTimeMillis()
    logger.debug("Running time: "+ (end - start)/1000.0)
    sys.exit(0)
  }
}

