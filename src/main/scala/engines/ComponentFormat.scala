package engines

import engines.coreast._

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by dxble on 6/9/16.
  */

class ComponentFormat(config: SynthesisConfig) {
  val compFormat = "%s %s %s\n"
  val BOOL = "Bool"
  val INT_EXPR = "IntExpr"
  val START = "Start"
  val INT = "Int"
  val currentBoolOp = new mutable.HashSet[String]()
  val currentArithOp = new mutable.HashSet[String]()
  var nullCheckPresence = false

  def this(exp: CAST, config: SynthesisConfig) ={
    this(config)
    currentOpOfExp(exp)
  }

  def currentBool(): String = {
    val curOps = currentBoolOp.foldLeft(new ArrayBuffer[String]){
      (res, op) => {
        op match {
          case ">" => {
            val gt = boolGT()
            if(!res.contains(gt))
              res.append(gt)
            res
          }
          case ">=" => {
            val gte = boolGTE()
            if(!res.contains(gte))
              res.append(gte)
            res
          }
          case "<" => {
            val lt = boolLT()
            if(!res.contains(lt))
              res.append(lt)
            res
          }
          case "<=" =>  {
            val lte = boolLTE()
            if(!res.contains(lte))
              res.append(lte)
            res
          }
          case "or" => {
            val or = boolOr()
            if(!res.contains(or))
              res.append(or)
            res
          }
          case "and" =>  {
            val and = boolAnd()
            if(!res.contains(and))
              res.append(and)
            res
          }
          case "=" => {
            val eq = boolEQ()
            if(!res.contains(eq))
              res.append(eq)
            res
          }
          case "not" => {
            val not = boolNot()
            if(!res.contains(not))
              res.append(not)
            res
          }
          case _ => throw new RuntimeException("Not yet handled operator: "+op)
        }
      }
    }
    curOps.foldLeft(""){(res, curOp) => res+curOp}
  }

  def boolAlternatives():String = {
    val alts = currentBoolOp.foldLeft(new ArrayBuffer[String]){
      (res, op) => {
        op match {
          case ">" => {
            val gte = boolGTE()
            val gt = boolGT()
            if(!res.contains(gte))
              res.append(gte)
            if(!res.contains(gt))
              res.append(gt)
            res
          }
          case ">=" => {
            val gte = boolGTE()
            val gt = boolGT()
            if(!res.contains(gt))
              res.append(gt)
            if(!res.contains(gte))
              res.append(gte)
            res
          }
          case "<" => {
            val lte = boolLTE()
            val lt = boolLT()
            if(!res.contains(lte))
              res.append(lte)
            if(!res.contains(lt))
              res.append(lt)
            res
          }
          case "<=" =>  {
            val lte = boolLTE()
            val lt = boolLT()
            if(!res.contains(lt))
              res.append(lt)
            if(!res.contains(lte))
              res.append(lte)
            res
          }
          case "or" => {
            val and = boolAnd()
            val or = boolOr()
            if(!res.contains(and))
              res.append(and)
            if(!res.contains(or))
              res.append(or)
            res
          }
          case "and" =>  {
            val and = boolAnd()
            val or = boolOr()
            if(!res.contains(or))
              res.append(or)
            if(!res.contains(and))
              res.append(and)
            res
          }
          case "=" => {
            val not = boolNot()
            val eq = boolEQ()
            if(!res.contains(not))
              res.append(not)
            if(!res.contains(eq))
              res.append(eq)
            res
          }
          case "not" => {
            val not = boolNot()
            val eq = boolEQ()
            if(!res.contains(eq))
              res.append(eq)
            if(!res.contains(not))
              res.append(not)
            res
          }
          case _ => throw new RuntimeException("Not yet handled operator: "+op)
        }
      }
    }
    if(nullCheckPresence || currentBoolOp.isEmpty){
      val not = boolNot()
      if(!alts.contains(not))
        alts.append(not)
    }
    alts.foldLeft(""){(res, alt) => res+alt}
  }

  def basicArithmetics(intKind: String): String = {
    val current = currentArith(intKind)
    val additionalOp = new ArrayBuffer[String]
    if(!currentArithOp.contains("+")){
      val plus = intPlus(INT_EXPR)
      additionalOp.append(plus)
    }

    if(!currentArithOp.contains("-")){
      val minus = intMinus(INT_EXPR)
      additionalOp.append(minus)
    }
    additionalOp.foldLeft(current){(res, add) => res+add}
  }

  def basicLogic(): String = {
    val current = currentBool()
    val additionalOp = new ArrayBuffer[String]
    if(!currentBoolOp.contains("and")){
      val and = boolAnd()
      additionalOp.append(and)
    }
    if(!currentBoolOp.contains("or")){
      val or = boolOr()
      additionalOp.append(or)
    }

    if(!currentBoolOp.contains("not")){
      val not = boolNot()
      additionalOp.append(not)
    }
    //Bach le: new updates
    if(!currentBoolOp.contains("=")){
      val eq = boolEQ()
      additionalOp.append(eq)
    }
    /*if(!currentBoolOp.contains(">")){
      val gt = boolGT()
      additionalOp.append(gt)
    }
    if(!currentBoolOp.contains(">=")){
      val gte = boolGTE()
      additionalOp.append(gte)
    }*/
    additionalOp.foldLeft(current){(res, add) => res+add}
  }

  def basicInequalities(): String = {
    val current = currentBool()
    val additionalOp = new ArrayBuffer[String]
    if(!currentBoolOp.contains("=")){
      val eq = boolEQ()
      additionalOp.append(eq)
    }
    if(!currentBoolOp.contains(">")){
      val gt = boolGT()
      additionalOp.append(gt)
    }
    if(!currentBoolOp.contains(">=")){
      val gte = boolGTE()
      additionalOp.append(gte)
    }
    additionalOp.foldLeft(current){(res, add) => res+add}
  }

  def arithmeticAlternatives(intKind: String): String = {
    val alts = currentArithOp.foldLeft(new ArrayBuffer[String]()){
      (res, op) => {
        op match {
          case "+" => {
            val minus = intMinus(intKind)
            val plus = intPlus(intKind)
            if(!res.contains(minus)){
              res.append(minus)
            }
            if(!res.contains(plus)){
              res.append(plus)
            }
            res
          }
          case "-" => {
            val minus = intMinus(intKind)
            val plus = intPlus(intKind)
            if(!res.contains(plus)){
              res.append(plus)
            }
            if(!res.contains(minus)){
              res.append(minus)
            }
            res
          }
          case "*" => {
            val mult = intMult(intKind)
            val div = intDiv(intKind)
            if(!res.contains(div)){
              res.append(div)
            }
            if(!res.contains(mult)){
              res.append(mult)
            }
            res
          }
          case "/" => {
            val mult = intMult(intKind)
            val div = intDiv(intKind)
            if(!res.contains(mult)){
              res.append(mult)
            }
            if(!res.contains(div)){
              res.append(div)
            }
            res
          }
          case _ => {
            println("Not yet handled operator: "+op)
            res
          }
        }
      }
    }
    alts.foldLeft(""){(res, alt) => res+alt}
  }

  def currentArith(intKind: String): String = {
    val alts = currentArithOp.foldLeft(new ArrayBuffer[String]()){
      (res, op) => {
        op match {
          case "+" => {
            val plus = intPlus(intKind)
            if(res.contains(plus)){
              res.append(plus)
            }
            res
          }
          case "-" => {
            val minus = intMinus(intKind)
            if(res.contains(minus)){
              res.append(minus)
            }
            res
          }
          case _ => res
        }
      }
    }
    alts.foldLeft(""){(res, alt) => res+alt}
  }

  // This should be called on the original buggy exp
  def currentOpOfExp(exp: CAST): Unit ={
    exp match {
      case e:BoolOperatorsCAST => {
        e match {
          case eBin: BinOp =>{
            currentBoolOp.add(eBin.topLevelString())
            currentOpOfExp(eBin.getLeft())
            currentOpOfExp(eBin.getRight())
          }
          case eUn: UnOp => {
            currentBoolOp.add(eUn.topLevelString())
            currentOpOfExp(eUn.getOperand())
          }
          case e: Dummy => {
            currentOpOfExp(e.getOp())
          }
          case e: BoolVar => {
            // There is a presence of null check, thus there should be "=" operator
            if(e.name.contains("NCK_NEQ"))
              nullCheckPresence = true
          }
          case _ => {}
        }
      }
      case e: ArithOperatorsCAST => {
        e match {
          case eBin: BinOp =>{
            currentArithOp.add(eBin.topLevelString())
            currentOpOfExp(eBin.getLeft())
            currentOpOfExp(eBin.getRight())
          }
          case eUn: UnOp => {
            currentArithOp.add(eUn.topLevelString())
            currentOpOfExp(eUn.getOperand())
          }
          case e: Dummy => {
            currentOpOfExp(e.getOp())
          }
          case _ => {}
        }
      }
      case e: Dummy => {
        currentOpOfExp(e.getOp())
      }
      case _ => {}
    }
  }

  private def boolLTE(): String={
    return compFormat.format("(<=",INT_EXPR,INT_EXPR+")")
  }

  private def boolGTE(): String={
    return compFormat.format("(>=",INT_EXPR,INT_EXPR+")")
  }

  private def boolGT(): String={
    return compFormat.format("(>",INT_EXPR,INT_EXPR+")")
  }

  private def boolLT(): String={
    return compFormat.format("(<",INT_EXPR,INT_EXPR+")")
  }

  private def boolEQ(): String={
    return compFormat.format("(=",INT_EXPR,INT_EXPR+")")
  }

  private def boolAnd(): String={
    return compFormat.format("(and",START,START+")")
  }

  private def boolOr(): String={
    return compFormat.format("(or",START,START+")")
  }

  def boolNot(): String={
    return compFormat.format("(not",START,")")
  }

  private def boolStart(): String={
    return compFormat.format("(Start",BOOL,"(")
  }

  private def boolConstants(): String={
    return compFormat.format("true","false","\n")
  }

  private def intPlus(intKind: String = INT_EXPR): String={
    return compFormat.format("(+",intKind,intKind+")")
  }

  private def intMinus(intKind: String = INT_EXPR): String={
    return compFormat.format("(-",intKind,intKind+")")
  }

  private def intMult(intKind: String = INT_EXPR): String={
    return compFormat.format("(*",intKind,intKind+")")
  }

  private def intDiv(intKind: String = INT_EXPR): String={
    return compFormat.format("(/",intKind,intKind+")")
  }

  private def intExpr(): String={
    return compFormat.format("(IntExpr",INT,"(")
  }

  private def intConstants(constants: String): String={
    return constants+"\n"
  }

  private def intVars(vars: String): String={
    return vars+"\n"
  }

  private def intStart(): String={
    return compFormat.format("(Start",INT,"(")
  }

  def boolStartDef(constants: String, boolVariables: String): String={
    if(config.componentLevel.isInstanceOf[Alternatives]){
      return boolStart()+boolAlternatives()+boolVariables+"\n"+constants+"\n"+"))\n"
    }else if(config.componentLevel.isInstanceOf[BasicInequalities]){
      return boolStart()+basicInequalities()+boolVariables+"\n"+constants+"\n"+"))\n"
    }else if(config.componentLevel.isInstanceOf[BasicLogic]){
      return boolStart()+basicLogic()+boolVariables+"\n"+constants+"\n"+"))\n"
    }
    //return boolStart()/*+boolLTE()*/+boolGTE()+boolGT()+/*boolLT()+*/boolEQ()+boolVariables+"\n"+constants+"\n"+boolNot()+boolOr()+boolAnd()+"))\n"
    return boolStart()+currentBool()+boolVariables+"\n"+constants+"\n"+"))\n"
  }

  def intOnlyDef(constants: String, variables: String): String={
    if(config.componentLevel.isInstanceOf[Alternatives]){
      return intStart()+arithmeticAlternatives(START)+intVars(variables)+intConstants(constants)+"))\n"
    }else if(config.componentLevel.isInstanceOf[BasicArithmetic]){
      return intStart()+basicArithmetics(START)+intVars(variables)+intConstants(constants)+"))\n"
    }
    //return intStart()+intVars(variables)+intConstants(constants)+intPlus(START)+intMinus(START)+"))\n"
    return intStart()+currentArith(START)+intVars(variables)+intConstants(constants)+"))\n"
  }

  private def intExprDef(constants: String, variables: String): String={
    if(config.componentLevel.isInstanceOf[Alternatives]){
      return intExpr()+arithmeticAlternatives(INT_EXPR)+intVars(variables)+intConstants(constants)+"))\n"
    }else if(config.componentLevel.isInstanceOf[BasicArithmetic]){
      return intExpr()+basicArithmetics(INT_EXPR)+intVars(variables)+intConstants(constants)+"))\n"
    }
    //return intExpr()+intVars(variables)+intPlus()+intMinus()+intConstants(constants)+"))\n"
    return intExpr()+currentArith(INT_EXPR)+intVars(variables)+intConstants(constants)+"))\n"
  }

  def boolIntDef(boolConstants: String, intConstants: String, intVariables: String, boolVariables: String): String={
    return boolStartDef(boolConstants, boolVariables)+intExprDef(intConstants,intVariables)
  }

  def boolOnlyDef(constants: String, boolVariables: String): String ={
    if(config.componentLevel.isInstanceOf[Alternatives]){
      return boolStart()+boolAlternatives()+boolVariables+"\n"+constants+"\n"+"))\n"
    }else if(config.componentLevel.isInstanceOf[BasicInequalities]){
      return boolStart()+basicInequalities()+boolVariables+"\n"+constants+"\n"+"))\n"
    }else if(config.componentLevel.isInstanceOf[BasicLogic]){
      return boolStart()+basicLogic()+boolVariables+"\n"+constants+"\n"+"))\n"
    }
    return boolStart()+boolVariables+"\n"+constants+"\n"+boolNot()+boolOr()+boolAnd()+"\n"+"))\n"
  }

  def main(args: Array[String]) {
    val const = "0 1"
    val boolConst = "true false"
    val variables = "a b"
    //println(boolStartDef())
    //println(intOnlyDef(const, variables))
    //println(boolIntDef(boolConst,const,variables))
    //println(boolOnlyDef(boolConst))
  }
}
