package extsolvers.common

import engines.coreast._

import scala.collection.mutable

/**
  * Created by dxble on 9/30/16.
  */
class VarsCollector extends CASTVisitorBase{
  val varNames = mutable.HashSet[String]()
  override def visitFunctionSignature(functionDesign: FunctionSignature): VisitorReturnType = ???

  override def visitBoolVar(boolVar: BoolVar): VisitorReturnType = {
    varNames.add(boolVar.name)
    BoolType(null, true)
  }

  override def visitIntVar(intVar: IntVar): VisitorReturnType = {
    varNames.add(intVar.name)
    IntType(null,1)
  }
}
