package extsolvers.metasearch.solutionclassifier.clustering.mcl;

/**
 * Created by dxble on 7/20/16.
 */
import java.util.*;


/**
 *
 * @author JiriKrizek
 */
public class MCClusterImpl {

    private Set<MCNode> nodes = new HashSet<MCNode>();
    private String clusterName = "untitled";
    private Integer metaNode = null;
    private int id;

    private boolean isDividable = false;

    public void setDividable(boolean dividable) {isDividable = dividable;}
    public boolean isDividable() {return isDividable;}

    public MCClusterImpl(int id) {
        this.id = id;
    }

    public MCClusterImpl(Set<MCNode> nodeList, int id) {
        this.nodes = nodeList;
        this.id = id;
    }

    @Override
    public int hashCode(){
        int code = 0;
        for(MCNode n: nodes){
            code += n.hashCode();
        }
        return code;
    }

    @Override
    public boolean equals(Object that) {
        if(that instanceof MCClusterImpl){
            MCClusterImpl castThat = (MCClusterImpl) that;
            for(MCNode n: nodes) {
                if (!castThat.getNodes().contains(n))
                    return false;
            }
            return true;
        }else{
            return false;
        }
    }

    public void addNode(MCNode node) {
        if(!this.nodes.contains(node)) {
            this.nodes.add(node);
        }
    }

    public void setName(String clusterName) {
        this.clusterName = clusterName;
    }

    /*public Integer[] getNodes() {
        Integer[] res = new Integer[this.nodes.size()];
        int pos = 0;

        Iterator<MCNode> i = this.nodes.iterator();
        while(i.hasNext()) {
            res[pos++] = i.next().getNode();
        }
        return res;
    }*/

    public Set<MCNode> getNodes() {
        return nodes;
    }

    public int getNodesCount() {
        return this.nodes.size();
    }

    public String getName() {
        return clusterName;
    }

    public Integer getMetaNode() {
        return this.metaNode;
    }

    public void setMetaNode(Integer node) {
        this.metaNode = node;
    }

    public int getId() {
        return this.id;
    }
}
