package synthlib2parser

/**
  * Created by dxble on 7/8/16.
  */

import scala.util.parsing.input.Positional

sealed trait Synthlib2Token extends Positional

case class TK_SETLOGIC() extends Synthlib2Token
case class TK_LPARENT() extends  Synthlib2Token
case class TK_RPARENT() extends  Synthlib2Token
case class TK_DEFINE_SORT() extends  Synthlib2Token
case class TK_DEFINE_FUN() extends  Synthlib2Token
case class TK_DECLARE_FUN() extends  Synthlib2Token
case class TK_CHECK_SYNTH() extends  Synthlib2Token
case class TK_DECLARE_VAR() extends  Synthlib2Token
case class TK_SYNTH_FUN() extends  Synthlib2Token
case class TK_CONSTRAINT() extends  Synthlib2Token
case class TK_ASSERT() extends  Synthlib2Token
case class TK_ORIGINALEXPR() extends  Synthlib2Token
case class TK_BV() extends  Synthlib2Token
case class TK_ARRAY() extends  Synthlib2Token
case class TK_INT() extends  Synthlib2Token
case class TK_BOOL() extends  Synthlib2Token
case class TK_ENUM() extends  Synthlib2Token
case class TK_REAL() extends  Synthlib2Token
case class TK_CONSTANT() extends  Synthlib2Token
case class TK_VARIABLE() extends  Synthlib2Token
case class TK_INPUT_VARIABLE() extends  Synthlib2Token
case class TK_LOCAL_VARIABLE() extends  Synthlib2Token
case class TK_LET() extends  Synthlib2Token
case class TK_DOUBLECOLON() extends  Synthlib2Token
case class TK_BOOL_LITERAL(str: String) extends  Synthlib2Token
case class TK_INT_LITERAL(str: String) extends  Synthlib2Token
case class TK_REAL_LITERAL(str: String) extends  Synthlib2Token
case class TK_BV_LITERAL(str: String) extends  Synthlib2Token
case class TK_SYMBOL(str: String) extends  Synthlib2Token
case class TK_QUOTED_LITERAL(str: String) extends  Synthlib2Token
case class DIGIT(str: String) extends  Synthlib2Token
case class INT_CONST(str: String) extends  Synthlib2Token

case class INDENTATION(spaces: Int) extends Synthlib2Token
case class INDENT() extends Synthlib2Token
case class DEDENT() extends Synthlib2Token