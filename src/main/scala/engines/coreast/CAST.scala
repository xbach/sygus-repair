package engines.coreast

import java.util.Random
import java.util.regex.Pattern

import engines.coreast.CASTNodeKind.CASTNodeKind
import synthlib2parser.ASTCmdKind._

import scala.collection.mutable.ArrayBuffer
import scala.util.parsing.input.{NoPosition, OffsetPosition, Position}
/**
  * Created by dxble on 6/21/16.
  */

object CASTNodeKind extends Enumeration {
  type CASTNodeKind = Value
  val BOOL_KIND,
  ARITHMETIC_KIND = Value
}
trait Dummy{
  def getOp(): CAST
}
trait Exp2Change extends Dummy{
}
trait ExpNoChange extends Dummy{
}

abstract class CAST extends VisitorReturnType{
  var position: Position = NoPosition

  protected def doAccept[T <: CAST](visitorBase: CASTVisitorBase, visit: T => VisitorReturnType, v: T): VisitorReturnType ={
    val (prevRes, toVisit) = visitorBase.preVisit(v)
    if(toVisit) {
      val res = visit(v)
      visitorBase.postVisit(v)
      res
    }else prevRes
  }

  def accept(visitorBase: CASTVisitorBase): VisitorReturnType
  def topLevelString(): String
  def smtString(): String
  def mangledString(): String = toString
  def size(): Int
  override def eq(that: VisitorReturnType): Boolean = this.equals(that)

  def getSort(): CASTNodeKind = {
    if(this.isInstanceOf[BoolOperatorsCAST])
      return CASTNodeKind.BOOL_KIND
    else if(this.isInstanceOf[ArithOperatorsCAST])
      return CASTNodeKind.ARITHMETIC_KIND
    else
      throw new RuntimeException("Not yet designed operator type!")
  }
  def setPosition(pos: Position) = position = pos
  def clonePosition(): Position ={
    if(this.position.line == 0 && this.position.column ==0)
      return NoPosition

    val clonedPos = new OffsetPosition(this.position.asInstanceOf[OffsetPosition].source, this.position.asInstanceOf[OffsetPosition].offset)
    clonedPos
  }

  def myClone(): CAST
}
trait InternalTerm extends CAST
case class FunctionSignature(funName: String, args: ArrayBuffer[InternalTerm]) extends CAST with InternalTerm{
  def this(funName: String, args: ArrayBuffer[InternalTerm], pos: Position){
    this(funName, args)
    position = pos
  }
  override def toString(): String = "("+funName+args.foldLeft(""){(res, arg) => res+" "+arg}+")"

  override def mangledString(): String = "("+funName+args.foldLeft(""){(res, arg) => res+" "+arg.mangledString()}+")"

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
      doAccept(visitorBase, visitorBase.visitFunctionSignature, this)
  }

  override def hashCode(): Int = {
    funName.hashCode + args.foldLeft(0){(res, arg) => {
      arg match {
        case BoolVar(_) => res + 1
        case IntVar(_) => res + 2
        case _ => res + 3 //Normally this is unexpected
      }
    }}
  }

  private def sameType(e1: InternalTerm, e2: InternalTerm): Boolean = {
    if((e1.isInstanceOf[BoolVar] && e2.isInstanceOf[BoolVar]) || (e1.isInstanceOf[IntVar] && e2.isInstanceOf[IntVar]) ||
      (e1.isInstanceOf[BoolConst] && e2.isInstanceOf[BoolConst]) || (e1.isInstanceOf[IntConst] && e2.isInstanceOf[IntConst]))
      return true
    else
      return false
  }

  /**
    * Check on function name, and types of each arguments. Note that we do not take into account arguments' name.
    * Instead, we only take into account arguments' sorts (types)
    *
    * @param that
    * @return true if two function has same name and same types of arguments
    */
  override def equals(that: Any): Boolean ={
    if(!that.isInstanceOf[FunctionSignature])
      return false
    val castThat = that.asInstanceOf[FunctionSignature]
    if(!funName.equals(castThat.funName))
      return false
    var i = 0
    while (i<args.size){
      if(!(sameType(args(i),castThat.args(i))))
        return false
      i += 1
    }
    return true
  }

  //Be careful with this. We don't count the function signature size.
  //We actually need the function body to get its size
  override def size(): Int = 0

  override def topLevelString(): String = funName

  override def smtString(): String = toString()

  override def myClone(): CAST = {
    val clonedArgs = args.foldLeft(new ArrayBuffer[InternalTerm]()){
      (res, arg) => {
        res.append(arg.myClone().asInstanceOf[InternalTerm])
        res
      }
    }
    new FunctionSignature(funName, clonedArgs, clonePosition())
  }
}
trait BinOp extends CAST{
  def getLeft(): CAST
  def getRight(): CAST
}
trait UnOp extends CAST{
  def getOperand(): CAST
}
abstract class BoolOperatorsCAST extends CAST{
  override def getSort(): CASTNodeKind = CASTNodeKind.BOOL_KIND
}
case class Greater(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST) extends BoolOperatorsCAST with BinOp{
  def this(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST, pos: Position){
    this(opr1, opr2)
    position = pos
  }

  override def toString() : String ={
    "("+opr1.toString+ " > "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+ " > "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
      doAccept(visitorBase,visitorBase.visitGreater, this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = ">"

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(> "+opr1.smtString()+" "+opr2.smtString+")"

  override def myClone(): CAST = new Greater(opr1.myClone().asInstanceOf[ArithOperatorsCAST], opr2.myClone().asInstanceOf[ArithOperatorsCAST], clonePosition())
}
case class LessThan(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST) extends BoolOperatorsCAST with BinOp{
  def this(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST, pos: Position){
    this(opr1, opr2)
    position = pos
  }

  override def toString() : String ={
    "("+opr1.toString+ " < "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+ " < "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitLessThan, this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = "<"

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(< "+opr1.smtString+ " "+opr2.smtString+")"

  override def myClone(): CAST = new LessThan(opr1.myClone().asInstanceOf[ArithOperatorsCAST], opr2.myClone().asInstanceOf[ArithOperatorsCAST], clonePosition())
}
case class LTE(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST) extends BoolOperatorsCAST with BinOp{
  def this(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST, pos: Position){
    this(opr1, opr2)
    position = pos
  }

  override def toString() : String ={
    "("+opr1.toString+ " <= "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+ " <= "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitLTE, this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = "<="

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(<= "+opr1.smtString+ " "+opr2.smtString+")"

  override def myClone(): CAST = new LTE(opr1.myClone().asInstanceOf[ArithOperatorsCAST], opr2.myClone().asInstanceOf[ArithOperatorsCAST], clonePosition())

}

case class GTE(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST) extends BoolOperatorsCAST with BinOp{
  def this(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST, pos: Position){
    this(opr1, opr2)
    position = pos
  }

  override def toString() : String ={
    "("+opr1.toString+ " >= "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+ " >= "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitGTE, this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = ">="

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(>= "+opr1.smtString+ " "+opr2.smtString+")"

  override def myClone(): CAST = new GTE(opr1.myClone().asInstanceOf[ArithOperatorsCAST], opr2.myClone().asInstanceOf[ArithOperatorsCAST], clonePosition())

}
case class Equal(opr1: CAST, opr2: CAST) extends BoolOperatorsCAST with BinOp{
  def this(opr1: CAST, opr2: CAST, pos: Position){
    this(opr1, opr2)
    position = pos
  }

  override def toString() : String ={
    "("+opr1.toString+" == "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+" == "+opr2.mangledString+")"
  }

  @throws(classOf[DivisionByZeroException])
  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitEqual, this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = "="

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String =  "(= "+opr1.smtString+" "+opr2.smtString+")"

  override def myClone(): CAST = new Equal(opr1.myClone(), opr2.myClone(), clonePosition())

}
case class Not(opr: BoolOperatorsCAST) extends BoolOperatorsCAST with UnOp{
  def this(opr: BoolOperatorsCAST, pos: Position){
    this(opr)
    position = pos
  }

  override def toString() : String ={
    "!("+opr.toString+")"
  }

  override def mangledString() : String ={
    // Normalize the not (a == b) to a != b
    // This avoid unnecessary actions when using GumTree to measure tree differences
    if(opr.isInstanceOf[Equal]){
      val opr1 = opr.asInstanceOf[Equal].opr1
      val opr2 = opr.asInstanceOf[Equal].opr2
      "("+opr1.mangledString+" != "+opr2.mangledString+")"
    }else {
      "!(" + opr.mangledString + ")"
    }
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitNot, this)
  }

  override def size(): Int = opr.size() + 1

  override def topLevelString(): String = "not"

  override def getOperand(): CAST = opr

  override def smtString(): String =  "(not "+opr.smtString()+")"

  override def myClone(): CAST = new Not(opr.myClone().asInstanceOf[BoolOperatorsCAST], clonePosition())

}
case class And(opr1: BoolOperatorsCAST, opr2: BoolOperatorsCAST) extends BoolOperatorsCAST with BinOp{
  def this(opr1: BoolOperatorsCAST, opr2: BoolOperatorsCAST, pos: Position){
    this(opr1, opr2)
    position = pos
  }

  override def toString() : String ={
    "("+opr1.toString+" && "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+" && "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitAnd, this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = "and"

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(and "+opr1.smtString()+" "+opr2.smtString()+")"

  override def myClone(): CAST = new And(opr1.myClone().asInstanceOf[BoolOperatorsCAST], opr2.myClone().asInstanceOf[BoolOperatorsCAST], clonePosition())

}
case class Or(opr1: BoolOperatorsCAST, opr2: BoolOperatorsCAST) extends BoolOperatorsCAST with BinOp{
  def this(opr1: BoolOperatorsCAST, opr2: BoolOperatorsCAST, pos: Position){
    this(opr1, opr2)
    position = pos
  }

  override def toString() : String ={
    "("+opr1.toString+" || "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+" || "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitOr, this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = "or"

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(or "+opr1.smtString+" "+opr2.smtString+")"

  override def myClone(): CAST = new Or(opr1.myClone().asInstanceOf[BoolOperatorsCAST], opr2.myClone().asInstanceOf[BoolOperatorsCAST], clonePosition())

}
case class Implies(opr1: BoolOperatorsCAST, opr2: BoolOperatorsCAST) extends BoolOperatorsCAST with BinOp{
  def this(opr1: BoolOperatorsCAST, opr2: BoolOperatorsCAST, pos: Position){
    this(opr1, opr2)
    position = pos
  }

  override def toString() : String ={
    "("+opr1.toString+" => "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+" => "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitImplies, this)
  }

  override def size(): Int = opr1.size() + opr2.size() + 1

  override def topLevelString(): String = "=>"

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(=> "+opr1.smtString+" "+opr2.smtString+")"

  override def myClone(): CAST = new Implies(opr1.myClone().asInstanceOf[BoolOperatorsCAST], opr2.myClone().asInstanceOf[BoolOperatorsCAST], clonePosition())

}
case class BoolVar(name: String) extends BoolOperatorsCAST with InternalTerm{
  def this(name: String, pos: Position){
    this(name)
    position = pos
  }

  override def toString() : String ={
    name
  }

  override def mangledString() : String ={
    NameMangling.mangleVariable(name)
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitBoolVar, this)
    //new BoolType(this, opr1.accept(visitorBase).asInstanceOf[BoolType].or(opr2.accept(visitorBase).asInstanceOf[BoolType]))
  }
  override def size(): Int = 1

  override def topLevelString(): String = name

  override def smtString(): String = toString()

  override def myClone(): CAST = new BoolVar(name, clonePosition())

  override def hashCode(): Int = name.hashCode

  override def equals(that: Any): Boolean = {
    if(!that.isInstanceOf[BoolVar])
      return false
    else{
      that.asInstanceOf[BoolVar].name.compareTo(name) == 0
    }
  }
}
case class BoolConst(value: String) extends BoolOperatorsCAST with InternalTerm{
  def this(value: String, pos: Position){
    this(value)
    position = pos
  }

  override def mangledString(): String = NameMangling.mangleVariable(value)

  override def toString() : String ={
    if(value.compareTo("true") == 0)
      return "1"
    else{
      return "0"
    }
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitBoolConst, this)
  }

  override def size(): Int = 1

  override def topLevelString(): String = value

  override def smtString(): String = toString()

  override def myClone(): CAST = new BoolConst(value, clonePosition())

  override def hashCode(): Int = value.hashCode

  override def equals(that: Any): Boolean = {
    if(!that.isInstanceOf[BoolConst])
      return false
    else{
      that.asInstanceOf[BoolConst].value.compareTo(value) == 0
    }
  }
}
case class BoolExprNoChange(op: BoolOperatorsCAST) extends BoolOperatorsCAST with ExpNoChange{
  override def toString() : String ={
    op.toString
  }

  override def mangledString() : String ={
    op.mangledString
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {op.accept(visitorBase)}
  override def size(): Int = op.size()

  override def topLevelString(): String = op.topLevelString()
  override def smtString(): String = op.smtString()

  override def myClone(): CAST = BoolExprNoChange(op.myClone().asInstanceOf[BoolOperatorsCAST])

  override def getOp(): CAST = op
}
case class BoolExpr2Change(op: BoolOperatorsCAST) extends BoolOperatorsCAST with Exp2Change{
  override def toString() : String ={
    op.toString
  }

  override def mangledString() : String ={
    op.mangledString
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = op.accept(visitorBase)
  override def size(): Int = op.size()

  override def topLevelString(): String = op.topLevelString()
  override def smtString(): String = op.smtString()

  override def myClone(): CAST = BoolExpr2Change(op.myClone().asInstanceOf[BoolOperatorsCAST])

  override def getOp(): CAST = op
}
abstract class ArithOperatorsCAST extends CAST{
  override def getSort(): CASTNodeKind = CASTNodeKind.ARITHMETIC_KIND
}
case class Plus(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST) extends ArithOperatorsCAST with BinOp{
  def this(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST, pos: Position){
    this(opr1, opr2)
    position = pos
  }

  override def toString() : String ={
    "("+opr1.toString+" + "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+" + "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase,visitorBase.visitPlus, this)
  }
  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = "+"

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(+ "+opr1.smtString+" "+opr2.smtString+")"

  override def myClone(): CAST = new Plus(opr1.myClone().asInstanceOf[ArithOperatorsCAST], opr2.myClone().asInstanceOf[ArithOperatorsCAST], clonePosition())

}
case class Minus(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST) extends ArithOperatorsCAST with BinOp{
  def this(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST, pos: Position){
    this(opr1, opr2)
    position = pos
  }

  override def toString() : String ={
    "("+opr1.toString+" - "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+" - "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase,visitorBase.visitMinus,this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = "-"

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(- "+opr1.smtString+" "+opr2.smtString+")"

  override def myClone(): CAST = new Minus(opr1.myClone().asInstanceOf[ArithOperatorsCAST], opr2.myClone().asInstanceOf[ArithOperatorsCAST], clonePosition())

}
case class Div(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST) extends ArithOperatorsCAST with BinOp{
  def this(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST, pos: Position){
    this(opr1, opr2)
    position = pos
  }

  override def toString() : String ={
    "("+opr1.toString+" / "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+" / "+opr2.mangledString+")"
  }

  @throws(classOf[DivisionByZeroException])
  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase,visitorBase.visitDiv, this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = "/"

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(/ "+opr1.smtString+" "+opr2.smtString+")"

  override def myClone(): CAST = new Div(opr1.myClone().asInstanceOf[ArithOperatorsCAST], opr2.myClone().asInstanceOf[ArithOperatorsCAST], clonePosition())

}
case class Mult(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST) extends ArithOperatorsCAST with BinOp{
  def this(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST, pos: Position){
    this(opr1, opr2)
    position = pos
  }

  override def toString() : String ={
    "("+opr1.toString+" * "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+" * "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitMult, this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = "*"

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(* "+opr1.smtString+" "+opr2.smtString+")"

  override def myClone(): CAST = new Mult(opr1.myClone().asInstanceOf[ArithOperatorsCAST], opr2.myClone().asInstanceOf[ArithOperatorsCAST], clonePosition())

}
case class IntVar(name: String) extends ArithOperatorsCAST with InternalTerm{

  def this(name: String, pos: Position){
    this(name)
    position = pos
  }

  override def mangledString(): String = {
    NameMangling.mangleVariable(name)
  }

  override def toString() : String ={
    name
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitIntVar, this)
  }

  override def size(): Int = 1

  override def topLevelString(): String = name

  override def smtString(): String = toString()

  override def myClone(): CAST = new IntVar(name, clonePosition())

  override def hashCode(): Int = name.hashCode

  override def equals(that: Any): Boolean = {
    if(!that.isInstanceOf[IntVar])
      return false
    else{
      that.asInstanceOf[IntVar].name.compareTo(name) == 0
    }
  }
}
case class IntConst(value: String) extends ArithOperatorsCAST with InternalTerm{
  def this(value: String, pos: Position){
    this(value)
    position = pos
  }

  override def toString() : String ={
    value
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitIntConst, this)
  }

  override def size(): Int = 1

  override def topLevelString(): String = value

  override def smtString(): String = toString()

  override def myClone(): CAST = new IntConst(value, clonePosition())

  override def hashCode(): Int = value.hashCode

  override def equals(that: Any): Boolean = {
    if(!that.isInstanceOf[IntConst])
      return false
    else{
      that.asInstanceOf[IntConst].value.compareTo(value) == 0
    }
  }

  override def mangledString(): String = NameMangling.mangleVariable(value)
}
case class IntExprNoChange(op: ArithOperatorsCAST) extends ArithOperatorsCAST with ExpNoChange{
  override def toString() : String ={
    op.toString
  }

  override def mangledString() : String ={
    op.mangledString()
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = op.accept(visitorBase)
  override def size(): Int = op.size()

  override def topLevelString(): String = op.topLevelString()

  override def smtString(): String = op.smtString()

  override def myClone(): CAST = IntExprNoChange(op.myClone().asInstanceOf[ArithOperatorsCAST])

  override def getOp(): CAST = op
}
case class IntExpr2Change(op: ArithOperatorsCAST) extends ArithOperatorsCAST with Exp2Change{
  override def toString() : String ={
    op.toString
  }

  override def mangledString() : String ={
    op.mangledString()
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = op.accept(visitorBase)
  override def size(): Int = op.size()

  override def topLevelString(): String = op.topLevelString()
  override def smtString(): String = op.smtString()

  override def myClone(): CAST = IntExpr2Change(op.myClone().asInstanceOf[ArithOperatorsCAST])

  override def getOp(): CAST = op
}
object NameMangling{
  import scala.collection.mutable

  var mangledVarKeyName: mutable.HashMap[String, String] = new mutable.HashMap[String, String]
  var mangledVarNameKey: mutable.HashMap[String, String] = new mutable.HashMap[String, String]
  var mangledVarPattern: Pattern = Pattern.compile("ENCODEVAR_\\d+")
  private val rand: Random = new Random(0)

  def mangleVariable(name: String): String = {
    val mangled: String = mangledVarNameKey.getOrElse(name, null)
    if (mangled == null) {
      val hc: String = Integer.toString(rand.nextInt(1000000))
      val key: String = "ENCODEVAR" + "_" + hc
      mangledVarKeyName.put(key, name)
      mangledVarNameKey.put(name, key)
      return key
    }
    else return mangled
  }
}