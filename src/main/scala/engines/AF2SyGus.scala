package engines

/**
  * Created by dxble on 6/17/16.
  */


import engines.AngelicFix._
import engines.utils.CASTUtils
import engines.coreast.CAST

import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, HashMap}

/**
  * Created by dxble on 6/7/16.
  */
class AF2SyGus(af: AngelicForest,config: SynthesisConfig,
               intConsts: scala.collection.immutable.HashMap[String, scala.collection.mutable.HashSet[String]],
               boolConsts: scala.collection.immutable.HashMap[String, scala.collection.mutable.HashSet[String]],
               isCVC4: Boolean, excludeVars: mutable.HashMap[String, List[String]],
               asts: mutable.HashMap[String, (CAST, mutable.HashSet[String], mutable.HashSet[String])]) {
  //val random = new Random()
  val funSignatures = new mutable.HashMap[String, String]()
  val funChangedParts = new mutable.HashMap[String, CAST]()
  val keepVars = new mutable.HashMap[String, mutable.HashSet[String]]()
  def convertAF2SyGus(): String ={

    val (funcDelcs, stmtConstMap, stmtVarNameTypes) = AF2FuncDecls()
    val varDeclsStr = varDecls(stmtVarNameTypes)
    val constraints = AF2Constraints_orig(stmtVarNameTypes)

    return "\n(set-logic  LIA)\n" + funcDelcs+ "\n" +varDeclsStr+"\n"+constraints+"\n"+ "(check-synth)\n"
    //return ""
  }

  def AF2Constraints_orig(stmtVarNameTypes: mutable.HashMap[String, mutable.HashMap[String, String]]): String ={
    //Angelic forest = map: test -> angelic paths
    val constraints = af.foldLeft(List[String]()){
      (res, af) =>{//each test -> its angelic paths
        //And constraints for all tests since all tests need to pass
        //OR constraints between angelic paths of one test, since one path passes can make that test pass
        //And constraints between angelic values of a path
        af match {
          case (testId, aps) =>{
            //traverse angelic value of angelic paths to make and constraints
            val orConstraints = aps.foldLeft(List[String]()){
              (orConstr, apath) =>{
                val andConstraints = apath.foldLeft(List[String]()){
                  (andConstr, av) =>{
                    if(av.context.size > 0)
                      andConstr :+ angelicValue2Constraints_orig(av, isCVC4, excludeVars, stmtVarNameTypes)
                    else
                      andConstr
                  }
                }
                orConstr :+ " "+makeAndConstraint(andConstraints)
              }
            }
            res :+ makeOrConstraint(orConstraints)
          }
        }
      }
    }
    //val contraintsForAllTests = constraints(constraints.length-1) //makeAndConstraint(constraints(constraints.length-1))
    //return "(constraint "+contraintsForAllTests+")"
    return constraints.foldLeft(""){
      (res, c) =>{
        if(c != "")
          res + "(constraint "+c+")\n"
        else
          res
      }
    }
  }


  def getNameAndType(av: VariableValue): (String, String) ={
    return av match {
      case IntVal (name, value) => {
        (name,"Int")
      }
      case BoolVal(name, value) => {
        (name,"Bool")
      }
      case _ => throw new RuntimeException("Not supported Match!")
    }
  }

  def collectVarNameTypeRetTypeAndIntValues(aglList: ArrayBuffer[AngelicValue], stmtID: String, excludeVars: mutable.HashMap[String, List[String]]) ={
    //Assuming same var name should have same type
    aglList.foldLeft((new mutable.HashMap[String, String](), new mutable.HashSet[String](), "",
      new scala.collection.immutable.HashMap[String, ArrayBuffer[String]])){
      (res, av) =>{
        res match {
          case (varNameType, typesSet, retType, intValues) => {
            val additionConsts = av.context.foldLeft(new ArrayBuffer[String]()) {
              (constArray,varVal) => {
                val (name, typeStr) = getNameAndType(varVal)
                typesSet.add(typeStr)
                if (!varNameType.contains(name))
                  varNameType += (name -> typeStr)
                //Collect Int consts values here
                varVal match {
                  case IntVal(name, value) => {
                    if(!constArray.contains(value) && !excludeVars.get(stmtID).getOrElse(new ArrayBuffer[String]()).contains(name)){
                      constArray.append(value.toString)
                    }
                  }
                  case _ => {}
                }
                constArray
              }
            }
            //Update the constants int values collected above
            val updatedIntValues = intValues.get(av.expression) match {
              case None => {
                intValues + (av.expression -> additionConsts)
              }
              case Some(existingConsts) => {
                existingConsts.appendAll(additionConsts)
                //res.updated(av.expression, existingConsts)
                intValues
              }
            }

            val (_,typeStr) = getNameAndType(av.value)
            if(retType == "" || retType.compareTo(typeStr) == 0) {
              typesSet.add(typeStr)
              (varNameType, typesSet, typeStr, updatedIntValues)
            }
            else
              throw new RuntimeException("Not consistent return type!")
          }
        }
      }
    }
  }

  def collectIntValuesFromAngelicValues(aglList: ArrayBuffer[AngelicValue]): scala.collection.immutable.HashMap[String, ArrayBuffer[String]] ={
    //Assuming same var name should have same type
    aglList.foldLeft(new scala.collection.immutable.HashMap[String, ArrayBuffer[String]]){
      (res, av) =>{
        val additionConsts = av.context.foldLeft(new ArrayBuffer[String]()){
          (constArray, eachCtxAV) =>{
            eachCtxAV match {
              case IntVal(name, value) => {
                if(!constArray.contains(value)){
                  constArray.append(value.toString)
                }
              }
            }
            constArray
          }
        }
        res.get(av.expression) match {
          case None => {
            res + (av.expression -> additionConsts)
          }
          case Some(existingConsts) => {
            existingConsts.appendAll(additionConsts)
            //res.updated(av.expression, existingConsts)
            res
          }
        }
      }
    }
  }

  def funcVarArgs2VarDecls(stmtVarNames: mutable.HashMap[String, ArrayBuffer[String]]): String ={
    stmtVarNames.foldLeft(""){
      (varDecls, eachFunc) =>{
        eachFunc match {
          case (stmtLine, varNames) => {
            varNames.foldLeft(""){
              (res, name) => res+name+"_"+stmtLine+" "
            }
          }
        }
      }
    }
  }

  def not2Exclude(stmtLine: String, varName: String): Boolean = {
    val var2Keep = keepVars.getOrElse(stmtLine, null)
    if(var2Keep != null)
      if(var2Keep.contains(varName))
        return true

    !excludeVars.get(stmtLine).getOrElse(List[String]()).contains(varName)
  }

  def AF2FuncDecls() ={

    //Angelic forest = map: test -> angelic paths
    val funcsWithCompleteSpecs = af.foldLeft(new HashMap[String, ArrayBuffer[AngelicValue]])((expClusters, kv) => {
      val testID = kv._1
      // map: expression *stmt line* -> [angelic value]
      kv._2.foreach(aglPath =>{
        aglPath.foreach(aglValue =>
          if(!expClusters.contains(aglValue.expression)){
            val algValList = new ArrayBuffer[AngelicValue]()
            algValList.append(aglValue)
            expClusters += (aglValue.expression -> algValList)
          }else{
            val aglValueList = expClusters.get(aglValue.expression).getOrElse(new ArrayBuffer[AngelicValue]())
            aglValueList.append(aglValue)
            expClusters.update(aglValue.expression, aglValueList)
          }
        )
      })
      expClusters
    })

    val stmtVarNameTypes = new mutable.HashMap[String, mutable.HashMap[String, String]]()
    val (funcdelcs, stmtConstsMap) = funcsWithCompleteSpecs.foldLeft("", new mutable.HashMap[String, ArrayBuffer[String]]()){
      (topRes, func) =>{
        val (allFunDecls, stmtIntConstsMap) = topRes
        func match {
          case (stmtLine, avList) =>{
            val (ast, _,_) = asts.get(stmtLine).getOrElse(null)
            if(af.size >= 1 && config.spaceReduction) {
              val changedPart = CASTUtils.getChangedPart(ast)
              val changedPartVars = CASTUtils.collectVars(changedPart)
              keepVars += stmtLine -> changedPartVars
              funChangedParts += (stmtLine -> changedPart)
            }else{
              val changedPartVars = CASTUtils.collectVars(ast)
              keepVars += stmtLine -> changedPartVars
              funChangedParts += stmtLine -> ast
            }

            val (varNameTypeList, typesSet, retType, additionalIntConsts) = collectVarNameTypeRetTypeAndIntValues(avList, stmtLine, excludeVars)

            val (intVarNames,boolVarNames,params) = varNameTypeList.foldLeft(("","","")){
              (res, nameType) =>{
                res match {
                  case (intVarNames,boolVarNames,funcArgs) =>{
                    if(not2Exclude(stmtLine,nameType._1))
                      if(nameType._2 == "Int")
                         (intVarNames + nameType._1 +" ",boolVarNames,funcArgs +" "+"("+nameType._1+" "+nameType._2+")")
                      else
                        (intVarNames, boolVarNames+ nameType._1 +" ",funcArgs +" "+"("+nameType._1+" "+nameType._2+")")
                    else
                      (intVarNames,boolVarNames,funcArgs)
                  }
                }
              }
            }

            var funcDecl = ""

            /**
              * This means this func need to have at least one parameter
              */
            if(intVarNames != "" || boolVarNames != "") {
              funcDecl += "synth-fun f_" + stmtLine
              funcDecl += " (" + params + ")" + " " + retType
              funcDecl += "\n\t("

              funSignatures += (stmtLine -> ("f_"+stmtLine+ " (" + params + ")" + " " + retType))
              //This means only add if this func/stmtLine has at least one parameter
              stmtVarNameTypes += (stmtLine -> varNameTypeList)

              additionalIntConsts.foreach(
                eachEntry => {
                  val (stmtID, consts) = eachEntry
                  val existingConsts = stmtIntConstsMap.get(stmtID).getOrElse(new ArrayBuffer[String]())
                  existingConsts.appendAll(consts)
                  stmtIntConstsMap.update(stmtID, existingConsts)
                }
              )

              val intConstsOldExprs = intConsts.getOrElse(stmtLine, new scala.collection.mutable.HashSet[String]())
              val intConstsFromAF = stmtIntConstsMap.getOrElse(stmtLine, new ArrayBuffer[String]()).foldLeft(new mutable.HashSet[String]()){
                (res, const) => {
                  res.add(const)
                  res
                }
              }
              val boolConstsOldExpr = boolConsts.get(stmtLine).getOrElse(new mutable.HashSet[String]()).foldLeft(""){
                (res, const) => {
                  res + " " + const
                }
              }
              //if(!config.componentLevel.isInstanceOf[Alternatives])
              //  intConstsFromAF.foreach(const => intConstsOldExprs.add(const))
              val containsBool = typesSet.contains("Bool")
              val containsInt = typesSet.contains("Int")


              val componentFormatter = new ComponentFormat(funChangedParts.getOrElse(stmtLine, null), config)
              if (containsBool && containsInt)
                funcDecl += componentFormatter.boolIntDef(syGusBoolConst(boolConstsOldExpr), syGusIntConst(intConstsOldExprs, intConstsFromAF), intVarNames, boolVarNames)
              else if (containsBool)
                funcDecl += componentFormatter.boolOnlyDef(syGusBoolConst(boolConstsOldExpr), boolVarNames )
              else if (containsInt) {
                funcDecl += componentFormatter.intOnlyDef(syGusIntConst(intConstsOldExprs, intConstsFromAF), intVarNames)
              }
              else
                throw new RuntimeException("Not yet handle other types!")

              funcDecl += "\n\t)"
              funcDecl = "(\n" + funcDecl + "\n)"
            }
            (allFunDecls + funcDecl, stmtIntConstsMap)
          }
        }
      }
    }

    (funcdelcs, stmtConstsMap, stmtVarNameTypes)
  }

  private def makeAndConstraint(nv: List[String]): String ={
    nv match {
      case Nil => ""
      case e::Nil => return e
      case hd::tl => return " (and "+hd+" "+makeAndConstraint(tl)+") "
    }
  }

  private def makeOrConstraint(nv: List[String]): String ={
    nv match {
      case Nil => ""
      case e::Nil => return e
      case hd::tl => return " (or "+hd+" "+makeOrConstraint(tl)+") "
    }
  }

  def angelicValue2Constraints_orig(aglValue: AngelicValue, isCVC4: Boolean, excludeVars: mutable.HashMap[String, List[String]],
                                    stmtVarNameTypes: mutable.HashMap[String, mutable.HashMap[String, String]]): String ={
    val nameValPairs = aglValue.context.foldLeft(List[String]())((res, v) =>{
      v match {
        case IntVal(name, value) => {
          if(not2Exclude(aglValue.expression,name))//!excludeVars.get(aglValue.expression).getOrElse(List[String]()).contains
            if(isCVC4)
              if(value>=0)
                res :+ "(= "+varDeclUnique(aglValue,name)+" "+value+")"
              else {
                val temp = 0 - value
                res :+ "(= "+varDeclUnique(aglValue,name)+" (- "+temp+"))"
              }
            else
              res :+ "(= "+varDeclUnique(aglValue,name)+" "+value+")"
          else
            res
        }
        case BoolVal(name, value) => {
          if(not2Exclude(aglValue.expression,name))//!excludeVars.get(aglValue.expression).getOrElse(List[String]()).contains
            res :+ "(= "+varDeclUnique(aglValue,name)+" "+value+")"
          else
            res
        }
        case _ => throw new RuntimeException("Not supported Match!")
      }
    })

    var constraints = makeAndConstraint(nameValPairs)
    val fCall = angelicValue2FuncCall(aglValue, excludeVars, stmtVarNameTypes)
    if(fCall != "") {
      //val expected = "(= " +fCall+ " " + variableValue2String(aglValue.value) + ")"
      val expected = "(= " +fCall+ " " + variableValue2StringWMinusNumber(aglValue.value, isCVC4) + ")"
      constraints = "(=> " + constraints + " " + expected + ")"
      return constraints
    }else
      return ""
  }

  def variableValue2StringWMinusNumber(aglValue: VariableValue, isCVC4: Boolean): String ={
    aglValue match {
      case IntVal(name, value) => {
          if(isCVC4)
            if(value>=0)
                value.toString
            else {
              val temp = value.toString.replace("-","")
              " (- "+temp+")"
            }
          else
            value.toString
      }
      case BoolVal(name, value) => {
          value.toString
      }
      case _ => throw new RuntimeException("Not supported Match!")
    }
  }

  def variableValue2String(v: VariableValue): String ={
    v match {
      case IntVal(_, value) => value.toString
      case BoolVal(_,value) => value.toString
      case _ => throw new RuntimeException("Not supported Match!")
    }
  }

  def angelicValue2FuncCall(aglValue: AngelicValue, excludeVars: mutable.HashMap[String, List[String]],
                            stmtVarNameTypes: mutable.HashMap[String, mutable.HashMap[String, String]]): String ={

    // We require this in order for the functions specs to be consistent,
    // e.g., defined func a b, then when calling should always be f a1 b1, instead of some f a1 b1 and some f b1 a1
    val funcSpecs = stmtVarNameTypes.get(aglValue.expression).getOrElse(new mutable.HashMap[String, String]()) //throw  new RuntimeException("Why cannot find stmtID?")
    val varNames = funcSpecs.foldLeft(""){
      (res, argTypePair) =>{
        val (name, _) = argTypePair
        if(not2Exclude(aglValue.expression,name))//!excludeVars.get(aglValue.expression).getOrElse(List[String]()).contains
          res + varDeclUnique(aglValue, name)+" "
        else
          res
      }
    }
    if(varNames != "")
      return "(f_"+aglValue.expression+" "+varNames+")"
    else
      return ""
  }

  def varDecls(stmtVarNameTypes: mutable.HashMap[String, mutable.HashMap[String, String]]): String ={
    return stmtVarNameTypes.foldLeft("")((res, pair) =>{
      pair match {
        case (stmtLine, varNameTypes) => {
          res + varNameTypes.foldLeft(""){
            (res1, v) =>{
              if(not2Exclude(stmtLine,v._1))
                res1 + "("+"declare-var"+" "+v._1+"_"+stmtLine+" "+v._2+")"+"\n"
              else
                res1
            }
          }

        }
      }
    })
  }

  private def varDeclUnique(aglV: AngelicValue, name: String): String ={
    return name+"_"+aglV.expression
  }

  def hasTypes(aglValue: AngelicValue): (Boolean,Boolean) ={
    val hasBoolType = aglValue.value match {
      case BoolVal(_,_) => true
      case _ => {
        aglValue.context.exists(v => v match {
          case BoolVal(_,_) => true
          case _ => false
        })
      }
    }

    val hasIntType = aglValue.value match {
      case IntVal(_,_) => true
      case _ => {
        aglValue.context.exists(v => v match {
          case IntVal(_,_) => true
          case _ => false
        })
      }
    }

    return (hasBoolType, hasIntType)
  }

  def syGusIntConst(intConstsOldExpr: scala.collection.mutable.HashSet[String], intConstsAF: scala.collection.mutable.HashSet[String]): String ={
    // Bachle: new updates: testing basic logic to include integer constants
    if(!config.componentLevel.isInstanceOf[IntegerConstants] && !config.componentLevel.isInstanceOf[BasicArithmetic] && !config.componentLevel.isInstanceOf[BasicLogic]){
      val constsFromOldExps = intConstsOldExpr.foldLeft(""){
        (res, i) =>{
          res+" "+i
        }
      }
      return constsFromOldExps
    }
    else {
      //uniqueConsts += "0"
      if(!config.componentLevel.isInstanceOf[BasicArithmetic])
        intConstsAF.foreach(c => intConstsOldExpr.add(c))
      intConstsOldExpr.add("0")
      intConstsOldExpr.add("1")
      val constsFromOldExps = intConstsOldExpr.foldLeft(""){
        (res, i) =>{
          res+" "+i
        }
      }
      return constsFromOldExps
    }
  }

  def syGusIntConst_Old_20Jan(intConstsOldExpr: scala.collection.mutable.HashSet[String], intConstsAF: scala.collection.mutable.Set[String]): String ={
    //if(!config.componentLevel.isInstanceOf[IntegerConstants])
    //  return ""

    if(config.componentLevel.isInstanceOf[Alternatives]) {
      val constsFromOldExps = intConstsOldExpr.foldLeft(""){
        (res, i) =>{
          res+" "+i
        }
      }
      return constsFromOldExps
    }
    else {
      //uniqueConsts += "0"
      intConstsAF.foreach(c => intConstsOldExpr.add(c))
      intConstsOldExpr.add("0")
      intConstsOldExpr.add("1")
      val constsFromOldExps = intConstsOldExpr.foldLeft(""){
        (res, i) =>{
          res+" "+i
        }
      }
      return constsFromOldExps
    }
  }

  def syGusBoolConst(boolConsts: String): String ={
    if(config.componentLevel.isInstanceOf[BooleanConstants])
      return "true false\n"
    else{
      return boolConsts+"\n"
    }
  }
}
