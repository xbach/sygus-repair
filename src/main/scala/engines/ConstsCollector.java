package engines;

import smtvisitor.DefaultVisitor;
import org.smtlib.IExpr;
import org.smtlib.impl.SMTExpr;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by dxble on 6/17/16.
 */

public class ConstsCollector extends DefaultVisitor<IExpr> {

    protected static IExpr.ISymbol TRUE  = new SMTExpr.Symbol("true");
    protected static IExpr.ISymbol FALSE  = new SMTExpr.Symbol("false");
    protected static IExpr.ISymbol AND   = new SMTExpr.Symbol("and");

    protected static IExpr.IStringLiteral CONDITION = new SMTExpr.StringLiteral("CONDITION", false);
    protected static IExpr.IStringLiteral CALL      = new SMTExpr.StringLiteral("CALL", false);
    protected static IExpr.IStringLiteral RHS       = new SMTExpr.StringLiteral("RHS", false);
    protected static IExpr.IStringLiteral BLOCK     = new SMTExpr.StringLiteral("BLOCK", false);

    public Set<String> intConsts = new HashSet<>();
    public Set<String> boolConsts = new HashSet<>();

    @Override
    public IExpr visit(IExpr.IAttributedExpr e) throws VisitorException {
        return e;
    }

    @Override
    public IExpr visit(IExpr.IBinaryLiteral e) throws VisitorException {
        return e;
    }

    @Override
    public IExpr visit(IExpr.IDecimal e) throws VisitorException {
        return e;
    }

    @Override
    public IExpr visit(IExpr.IError e) throws VisitorException {
        return e;
    }

    @Override
    public IExpr visit(IExpr.IExists e) throws VisitorException {
        return e;
    }

    private boolean isBoolOperator(String op){
        //op == "=" || op == ">" || op == ">=" || op == "<" || op == "<=" are int operators
        if (op.compareTo("and") == 0 || op.compareTo("or") == 0 || op.compareTo("not") == 0 || op.compareTo("=>") == 0) return true;
        else return false;
    }

    @Override
    public IExpr visit(IExpr.IFcnExpr e) throws VisitorException {
        for(int i =0; i< e.args().size();i++){
            IExpr expr = e.args().get(i);
            if(expr instanceof IExpr.IFcnExpr)
                visit((IExpr.IFcnExpr)expr);
            else if(expr instanceof IExpr.ISymbol){
                //visit((IExpr.ISymbol) expr);
                String strValue = e.toString();
                if(strValue.compareTo("true") == 0 || strValue.compareTo("false") == 0){
                    boolConsts.add(strValue);
                }
            }
            else if(expr instanceof IExpr.INumeral){
                //visit((IExpr.INumeral) expr);
                if(!isBoolOperator(e.head().toString()))
                    intConsts.add(expr.toString());
                else{
                    if(expr.toString() == "0"){
                        boolConsts.add("false");
                    }else{
                        boolConsts.add("true");
                    }
                }
            }
            else
                System.out.println(expr.kind());
        }
        return e;
    }

    @Override
    public IExpr visit(IExpr.IForall e) throws VisitorException {
        return e;
    }

    @Override
    public IExpr visit(IExpr.IHexLiteral e) throws VisitorException {
        return e;
    }

    @Override
    public IExpr visit(IExpr.ILet e) throws VisitorException {
        return e;
    }

    @Override
    public IExpr visit(IExpr.INumeral e) throws VisitorException {
        intConsts.add(e.toString());
        return e;
    }

    @Override
    public IExpr visit(IExpr.IParameterizedIdentifier e) throws VisitorException {
        return e;
    }

    @Override
    public IExpr visit(IExpr.IAsIdentifier e) throws VisitorException {
        return e;
    }

    @Override
    public IExpr visit(IExpr.IStringLiteral e) throws VisitorException {
        return e;
    }

    @Override
    public IExpr visit(IExpr.ISymbol e) throws VisitorException {
        return e;
    }

}
