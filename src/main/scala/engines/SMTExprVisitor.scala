package engines

/**
  * Created by dxble on 6/21/16.
  */

import smtvisitor.DefaultVisitor
import org.smtlib.IExpr
import org.smtlib.impl.SMTExpr

import engines.coreast._
import org.smtlib.IVisitor.VisitorException

import scala.collection.mutable.ArrayBuffer

object SMTExprVisitor {
  protected var TRUE: IExpr.ISymbol = new SMTExpr.Symbol("true")
  protected var FALSE: IExpr.ISymbol = new SMTExpr.Symbol("false")
  protected var AND: IExpr.ISymbol = new SMTExpr.Symbol("and")
  protected var CONDITION: IExpr.IStringLiteral = new SMTExpr.StringLiteral("CONDITION", false)
  protected var CALL: IExpr.IStringLiteral = new SMTExpr.StringLiteral("CALL", false)
  protected var RHS: IExpr.IStringLiteral = new SMTExpr.StringLiteral("RHS", false)
  protected var BLOCK: IExpr.IStringLiteral = new SMTExpr.StringLiteral("BLOCK", false)
}

class SMTExprVisitor extends DefaultVisitor[CAST] {
  var ast: CAST = null
  var visitingBool: Boolean = false

  @throws[VisitorException]
  override def visit(e: IExpr.IAttributedExpr): CAST = {
    return null
  }

  @throws[VisitorException]
  override def visit(e: IExpr.IBinaryLiteral): CAST = {
    return null
  }

  @throws[VisitorException]
  override def visit(e: IExpr.IDecimal): CAST = {
    return null
  }

  @throws[VisitorException]
  override def visit(e: IExpr.IError): CAST = {
    return null
  }

  @throws[VisitorException]
  override def visit(e: IExpr.IExists): CAST = {
    return null
  }

  private def isBoolOperator(op: String): Boolean = {
    //op == "=" || op == ">" || op == ">=" || op == "<" || op == "<=" are int expr
    if (op == "and" || op == "or" || op == "not") return true
    else return false
  }

  @throws[VisitorException]
  override def visit(e: IExpr.IFcnExpr): CAST = {
    //System.out.println(e.pos())
    val grammarDesign: ArrayBuffer[CAST] = new ArrayBuffer [CAST]()
    var i: Int = 0
    while (i < e.args.size) {
        val expr: IExpr = e.args.get(i)
        if (expr.isInstanceOf[IExpr.IFcnExpr]) grammarDesign.append(visit(expr.asInstanceOf[IExpr.IFcnExpr]))
        else if (expr.isInstanceOf[IExpr.ISymbol]) {
          if (isBoolOperator(e.head.toString)) grammarDesign.append(new BoolVar(expr.toString))
          else grammarDesign.append(new IntVar(expr.toString))
        }
        else if (expr.isInstanceOf[IExpr.INumeral]) {
          if (isBoolOperator(e.head.toString)) grammarDesign.append(new BoolConst(expr.toString))
          else grammarDesign.append(new IntConst(expr.toString))
        }
        else System.out.println(expr.kind)
        i += 1;
    }
    if (e.head.toString == "and") {
      ast = new And(grammarDesign(0).asInstanceOf[BoolOperatorsCAST], grammarDesign(1).asInstanceOf[BoolOperatorsCAST])
    }
    else if (e.head.toString == "or") {
      ast = new Or(grammarDesign(0).asInstanceOf[BoolOperatorsCAST], grammarDesign(1).asInstanceOf[BoolOperatorsCAST])
    }
    else if (e.head.toString == "not") {
      ast = new Not(grammarDesign(0).asInstanceOf[BoolOperatorsCAST])
    }
    else if (e.head.toString == ">") {
      ast = new Greater(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST])
    }
    else if (e.head.toString == "<") {
      ast = new LessThan(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST])
    }
    else if (e.head.toString == ">=") {
      ast = new GTE(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST])
    }
    else if (e.head.toString == "<=") {
      ast = new LTE(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST])
    }
    else if (e.head.toString == "=") {
      ast = new Equal(grammarDesign(0), grammarDesign(1))
    }
    else if (e.head.toString == "+") {
      ast = new Plus(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST])
    }
    else if (e.head.toString == "-") {
      ast = new Minus(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST])
    }
    else if (e.head.toString == "/") {
      ast = new Div(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST])
    }
    else if (e.head.toString == "*") {
      ast = new Mult(grammarDesign(0).asInstanceOf[ArithOperatorsCAST], grammarDesign(1).asInstanceOf[ArithOperatorsCAST])
    }
    return ast
  }

  @throws[VisitorException]
  override def visit(e: IExpr.IForall): CAST = {
    return null
  }

  @throws[VisitorException]
  override def visit(e: IExpr.IHexLiteral): CAST = {
    return null
  }

  @throws[VisitorException]
  override def visit(e: IExpr.ILet): CAST = {
    return null
  }

  @throws[VisitorException]
  override def visit(e: IExpr.INumeral): CAST = {
    ast = new IntConst(e.toString)
    return ast
  }

  @throws[VisitorException]
  override def visit(e: IExpr.IParameterizedIdentifier): CAST = {
    return null
  }

  @throws[VisitorException]
  override def visit(e: IExpr.IAsIdentifier): CAST = {
    return null
  }

  @throws[VisitorException]
  override def visit(e: IExpr.IStringLiteral): CAST = {
    return null
  }

  @throws[VisitorException]
  override def visit(e: IExpr.ISymbol): CAST = {
    val strValue = e.toString
    if(strValue.compareTo("true") == 0 || strValue.compareTo("false") == 0){
      ast = new BoolConst(strValue)
    }
    else{
      ast = new BoolVar(strValue)
    }
    return ast
  }
}